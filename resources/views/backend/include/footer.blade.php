<div class="page-footer">
    <div class="container">
        &copy; 2015 <a href="http://bangkoksolutions.com/" target="_blank">Bangkok Solutions</a>, all rights reserved.
    </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>