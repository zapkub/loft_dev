<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                {{--<a href="{{URL::to('_admin')}}"><img src="{{ URL::asset('assets/admin/layout3/img/logo_bkksol.jpg') }}" alt="logo" class="logo-default"></a>--}}

                <a href="{{URL::to('_admin')}}"><img src="{{ URL::asset('assets/images/logo.gif') }}" alt="logo" class="logo-default"></a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu hidden-xs">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" >
                            <span class="username username-hide-mobile">Hello, {{auth()->guard('admin')->user()->firstname}}</span>
                        </a>
                    </li>
                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="{{URL::to('_admin/logout')}}" >
                            <span class="username username-hide-mobile"><i class="fa fa-sign-out"></i> Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">

            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">
                   {{-- <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            <i class="fa fa-reorder"></i> Manage Data <i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu pull-left">

                            <li class=" dropdown-submenu">
                                <a href="javascript:;"><i class="fa fa-tags"></i> New Template</a>
                                <ul class="dropdown-menu">
                                    <li class=""><a href="{{ URL::to('/_admin/new_page') }}"><i class="fa fa-tags"></i> Template</a></li>
                                </ul>
                            </li>--}}
                            {{--<li class=""><a href="{{ URL::to('/_admin/product') }}"><i class="fa fa-tags"></i> Product</a></li>--}}
                            {{--<li class=""><a href="{{ URL::to('/_admin/option_group') }}"><i class="fa fa-tags"></i> Option Group</a></li>--}}
                            {{--<li class=""><a href="{{ URL::to('/_admin/user') }}"><i class="fa fa-tags"></i> User</a></li>--}}
                            {{--<li class=""><a href="{{ URL::to('/_admin/address_book') }}"><i class="fa fa-tags"></i>Address</a></li>--}}
                            {{--<li class=""><a href="{{ URL::to('/_admin/orders') }}"><i class="fa fa-tags"></i>Orders</a></li>--}}
                            {{--<li class=""><a href="{{ URL::to('/_admin/category') }}"><i class="fa fa-tags"></i>Category</a></li>--}}
                            {{--<li class=""><a href="{{ URL::to('/_admin/product_category') }}"><i class="fa fa-tags"></i>Product Category</a></li>--}}
                            {{--<li class=""><a href="{{ URL::to('/_admin/manufacturer') }}"><i class="fa fa-tags"></i>Manufacturer</a></li>--}}
                            {{--<li class=""><a href="{{ URL::to('/_admin/special') }}"><i class="fa fa-tags"></i>Special</a></li>--}}


                        {{--</ul>--}}

                    {{--</li>--}}

                    <li><a href="{{ URL::to('_admin/category') }}"><i class="fa fa-th-list"></i> Category</a></li>
                    <li><a href="{{ URL::to('_admin/product_category') }}"><i class="fa fa-th-list"></i> Product Category</a></li>

                    <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            <i class="fa fa-shopping-cart"></i> Product <i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu pull-left">


                            <li><a href="{{ URL::to('_admin/product') }}"><i class="fa fa-shopping-cart"></i> Product</a></li>
                            <li><a href="{{ URL::to('_admin/discount') }}"><i class="fa fa-shopping-cart"></i> Discount</a></li>
                            <li><a href="{{ URL::to('_admin/discount_code') }}"><i class="fa fa-shopping-cart"></i> Discount Code</a></li>

                            </li>
                        </ul>
                    </li>

                    <li><a href="{{ URL::to('_admin/option_group') }}"><i class="fa fa-plus-circle"></i> Option Group</a></li>
                    <li><a href="{{ URL::to('_admin/manufacturer') }}"><i class="fa fa-navicon "></i> Manufacturer</a></li>
                    <li><a href="{{ URL::to('_admin/special') }}"><i class="fa fa-navicon "></i> Special</a></li>
                    <li><a href="{{ URL::to('_admin/address_book') }}"><i class="fa fa-home"></i> Address</a></li>
                    <li><a href="{{ URL::to('_admin/orders') }}"><i class="fa fa-file"></i> Orders</a></li>
                    <li><a href="{{ URL::to('_admin/user') }}"><i class="fa fa-user"></i> User</a></li>
                    <li><a href="{{ URL::to('_admin/banner_promotion') }}"><i class="fa fa-bold"></i> Banner Promotion</a></li>
                    <li><a href="{{ URL::to('_admin/about_us/1/edit') }}"><i class="fa fa-at"></i> About us</a></li>
                    <li><a href="{{ URL::to('_admin/contact_us/1/edit') }}"><i class="fa fa-at"></i> Contact us</a></li>
                    <li><a href="{{ URL::to('_admin/contact_msg') }}"><i class="fa fa-envelope-o"></i> Contact MSG</a></li>
                    <li><a href="{{ URL::to('_admin/new_page') }}"><i class="fa fa-envelope-o"></i> New Page</a></li>
                    <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            <i class="fa fa-tags"></i> Tags <i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu pull-left">

                            <li><a href="{{ URL::to('_admin/tag') }}"><i class="fa fa-angle-right"></i> Tags</a></li>
                            <li><a href="{{ URL::to('_admin/product_tag') }}"><i class="fa fa-angle-right"></i> Product Tag</a></li>
                            <li><a href="{{ URL::to('_admin/blog_tag') }}"><i class="fa fa-angle-right"></i> Blog Tag</a></li>

                            </li>
                        </ul>
                    </li>

                    <li><a href="{{ URL::to('_admin/blog') }}"><i class="fa fa-bold"></i> Blog</a></li>


                    <li class="visible-xs-block"><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li>

                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
