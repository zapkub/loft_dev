<?php
use App\Http\Controllers\Backend\BannerPromotionController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new BannerPromotionController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);


?>

@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-book" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">


                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title [TH]</label>
                                            <div class="col-md-4">
                                                <input type="text" name="title_th"  class="form-control" value="{{ $title_th }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title [EN]</label>
                                            <div class="col-md-4">
                                                <input type="text" name="title_en"  class="form-control" value="{{ $title_en }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Sub Title [TH]</label>
                                            <div class="col-md-4">
                                                <input type="text" name="sub_title_th"  class="form-control" value="{{ $sub_title_th }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Sub Title [EN]</label>
                                            <div class="col-md-4">
                                                <input type="text" name="sub_title_en"  class="form-control" value="{{ $sub_title_en }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group "> {{--tess P.--}}
                                        <div class="controls">
                                            <div id="thumbnail" class="text-center">
                                                @if($img_name != '')
                                                    <img src="<?php echo URL::asset('uploads/banner_promotion/'.$img_name) ?>" id="img_path2" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path2" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_path" id="img_path" value="<?php echo  $img_name?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Image</label>
                                            <div class="col-md-4">
                                                @if($img_name != '')
                                                    <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                    <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($img_name == '')
                                                    <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="control-label col-md-3">Show Date</label>
                                        <div class="col-md-4">
                                            <input type='text' name="show_date" id="show_date" class="form-control" value="{{ $show_date }}" data-date-format="yyyy-mm-dd hh:ii" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Start Date</label>
                                        <div class="col-md-4">
                                            <input type='text' name="start_date" id="start_date" class="form-control" value="{{ $start_date }}" data-date-format="yyyy-mm-dd hh:ii" readonly>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3">Stop Date</label>
                                        <div class="col-md-4">
                                            <input type='text' name="stop_date" id="stop_date" class="form-control" value="{{ $stop_date }}" data-date-format="yyyy-mm-dd hh:ii" readonly>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Link</label>
                                            <div class="col-md-4">
                                                <input type="text" name="link" id="link"  class="form-control" value="{{ $link }}"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status</label>
                                            <div class="col-md-4">

                                                <select name="status" class="form-control select2-container form-control select2me">

                                                    <option value="0" @if($status == 0) selected @endif >Close</option>
                                                    <option value="1"@if($status == 1) selected @endif >Open</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>



                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script>
        $(function() {
            $( "#show_date,#start_date,#stop_date" ).datetimepicker();

        });
    </script>

@endsection
