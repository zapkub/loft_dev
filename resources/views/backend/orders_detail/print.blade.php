<?php
use App\Http\Controllers\Backend\OrdersDetailController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new OrdersDetailController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$pkField2 = $objCon->pkField2;

$fieldList = $objCon->fieldList;

if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

?>

@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/orders')}}">Order</a></h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <?php
                                $user = DB::Table('user')
                                ->where('user_id',$data->user_id)
                                ->get();

                        ?>

                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="portlet light">
                            <div class="portlet-body">
                                <div class="invoice">
                                    <div class="row invoice-logo">
                                        <div class="col-xs-6 invoice-logo-space">
                                            <img src="{{ URL::asset('assets/images/logo.gif') }}" class="img-responsive" alt=""/>
                                        </div>
                                        <div class="col-xs-6">
                                            <p>
                                                #{{ $data->orders_id }} / {{ $mainFn->format_date_en($data->orders_date,4) }}
                                            </p>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <h2><strong>Client</strong></h2>
                                            <ul class="list-unstyled">
                                                @foreach($user as $value)
                                                <li>
                                                    {{ $value->firstname }} {{ $value->lastname }}
                                                </li>
                                                <li>
                                                    {{ $value->email }}
                                                </li>
                                                <li>
                                                    {{ $value->mobile }}
                                                </li>

                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-xs-4">
                                            {{--<h3>About:</h3>--}}
                                            {{--<ul class="list-unstyled">--}}
                                                {{--<li>--}}
                                                    {{--Drem psum dolor sit amet--}}
                                                {{--</li>--}}

                                            {{--</ul>--}}
                                        </div>
                                        <div class="col-xs-4 invoice-payment">
                                            <h2><strong>Payment By</strong></h2>
                                            <ul class="list-unstyled">
                                                <li>
                                                    {{ $data->payment_by }}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Item
                                                    </th>
                                                    <th class="hidden-480">
                                                        Quantity
                                                    </th>
                                                    <th class="hidden-480">
                                                        Shipped
                                                    </th>
                                                    <th class="hidden-480">
                                                        Unit Cost
                                                    </th>
                                                    <th>
                                                        Total
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php
                                                $orders_detail = DB::Table('orders_detail')
                                                        ->where('orders_id',$data->orders_id)
                                                        ->get();

                                                $total = 0;
                                                ?>

                                                @foreach($orders_detail as $value)

                                                <tr>
                                                    <td>
                                                        {{ $value->orders_detail_id }}
                                                    </td>
                                                    <td>
                                                        {{ $value->product_name }}
                                                    </td>
                                                    <td class="hidden-480">
                                                        {{ $value->qty }}
                                                    </td>
                                                    <td class="hidden-480">
                                                        {{ $value->shipped }}
                                                    </td>
                                                    <td class="hidden-480">
                                                        {{ $value->unit_price }}
                                                    </td>

                                                    <?php
                                                        $total_price = $value->shipped*$value->unit_price;
                                                        $total += $total_price;
                                                    ?>

                                                    <td class="hidden-480">
                                                        {{ $total_price }}

                                                    </td>
                                                </tr>

                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="well">
                                                <address>
                                                    <strong>Billing Address</strong><br/>
                                                    {{ $data->billing_address }}<br/>

                                                    <strong>Shipping Address</strong><br/>
                                                    {{ $data->shipping_address }}<br/>
                                                </address>
                                            </div>
                                        </div>
                                        <div class="col-xs-8 invoice-block text-right">
                                            <ul class="list-unstyled amounts">
                                                <li>
                                                    <strong>Sub - Total amount:</strong> {{ $total }}
                                                </li>
                                                <li>
                                                    <strong>Discount:</strong> {{ $data->discount }}
                                                </li>
                                                <li>
                                                    <strong>Grand Total:</strong> {{ $total-$data->discount }}
                                                </li>
                                            </ul>
                                            <br/>
                                            <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                                                Print <i class="fa fa-print"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
            <!-- END PAGE CONTAINER -->

        </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->


    </div>
    </div>
    <!-- END PAGE CONTENT -->
    </div>
@endsection