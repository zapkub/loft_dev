<?php
use App\Http\Controllers\Backend\OrdersDetailController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new OrdersDetailController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$pkField2 = $objCon->pkField2;

$fieldList = $objCon->fieldList;

if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

?>

@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/orders')}}">Order</a> / {{ $subtitle->orders_id}} </h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->


            <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->


                        <div class="portlet light">


                            <div class="portlet-title">
                                Order Detail
                            </div>

                            <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-product" class="form-horizontal">
                                <input name="_method" type="hidden" value="{{$method}}">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-shopping-cart"></i> Order No. {{ $subtitle->orders_id }}
                                        </div>
                                        <div class="actions">
                                            <a href="{{ URL::to('_admin/orders_detail/'.input::get('orders_id')) }}" target="_blank"  class="btn btn-circle yellow btn-xs"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered table-striped">
                                                <thead>
                                                <tr class="text-center">
                                                    <td class="col-md-9 text-left"><strong>Product Name</strong></td>
                                                    <td><strong>Price</strong></td>
                                                    <td><strong>Qty</strong></td>
                                                    <td><strong>Qty Ship</strong></td>
                                                    <td><strong>Ship</strong></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $data = DB::table('orders')
                                                        ->LeftJoin('orders_detail','orders.orders_id','=','orders_detail.orders_id')
                                                        ->where('orders.orders_id',Input::get('orders_id'))
                                                        ->get();

                                                $total = 0;
                                                ?>
                                                @foreach($data as $field)
                                                    <tr class="text-center">
                                                        <td class="text-left">{!! Html::decode($field->product_name) !!}</td>
                                                        <td>{!! Html::decode($field->unit_price) !!}</td>
                                                        <td>{!! Html::decode($field->qty) !!}</td>
                                                        <td>{!! Html::decode($field->shipped) !!}</td>
                                                        <td>

                                                            <?php
                                                                $status = DB::Table('status')->where('status_id',$field->status_id)->first();
                                                            ?>

                                                            @if($field->qty == $field->shipped) {{ $status->status_name }}
                                                            @else
                                                                <input type="text" name="shipped[{{ $field->orders_detail_id }}]" class="form-control" style="width: 50px;">
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach


                                                <input type="hidden" name="orders_id" class="form-control" value="{{ Input::get('orders_id') }}">
                                                @if($field->qty != $field->shipped)

                                                    <tr>
                                                        <td colspan="5" class="text-right"><button type="submit" class="btn green">Save</button></td>
                                                    </tr>

                                                @endif

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        {{--<div class="row">--}}
                            {{--<div class="col-md-6">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<div class="well">--}}
                                    {{--<div class="row static-info align-reverse">--}}
                                        {{--<div class="col-md-8 name">Sub Total:</div>--}}
                                        {{--<div class="col-md-3 value">{{ $total }}</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="row static-info align-reverse">--}}
                                        {{--<div class="col-md-8 name">Grand Total:</div>--}}
                                        {{--<div class="col-md-3 value"></div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>



                <?php
                $data = DB::table('orders')
                        ->where('parent_orders',Input::get('orders_id'))
                        ->get();
                ?>
                @foreach($data as $value)

                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->


                    <div class="portlet light">

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            # {{ $subtitle->orders_id }} - {{ $value->orders_id }}
                                        </div>
                                        <div class="actions">
                                            <a href="{{ URL::to('_admin/orders_detail/'.$value->orders_id) }}" target="_blank"  class="btn btn-circle yellow btn-xs"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered table-striped">
                                                <tr class="text-center">
                                                    <td class="col-md-9 text-left"><strong>Product Name</strong></td>
                                                    <td><strong>Price</strong></td>
                                                    <td><strong>Qty</strong></td>
                                                    <td><strong>Qty Ship</strong></td>
                                                    {{--<td><strong>Status</strong></td>--}}
                                                </tr>

                                                <?php
                                                $data = DB::table('orders_detail')
                                                        ->where('orders_id',$value->orders_id)
                                                        ->get();
                                                ?>
                                                @foreach($data as $field)
                                                    <tr class="text-center">
                                                        <td class="text-left">{!! Html::decode($field->product_name) !!}</td>
                                                        <td>{!! Html::decode($field->unit_price) !!}</td>
                                                        <td>{!! Html::decode($field->qty) !!}</td>
                                                        <td>{!! Html::decode($field->shipped) !!}</td>

                                                        {{--<td>--}}
                                                        {{--@if($field->qty == $field->shipped)Complete--}}
                                                        {{--@else Wait--}}
                                                        {{--@endif--}}
                                                        {{--</td>--}}
                                                    </tr>
                                                @endforeach

                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                    {{--<div class="well">--}}
                    {{--<div class="row static-info align-reverse">--}}
                    {{--<div class="col-md-8 name">Sub Total:</div>--}}
                    {{--<div class="col-md-3 value">{{ $total }}</div>--}}
                    {{--</div>--}}
                    {{--<div class="row static-info align-reverse">--}}
                    {{--<div class="col-md-8 name">Grand Total:</div>--}}
                    {{--<div class="col-md-3 value"></div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>

                @endforeach

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->


    </div>
    </div>
    <!-- END PAGE CONTENT -->
    </div>
@endsection