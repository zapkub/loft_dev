<?php
use App\Http\Controllers\Backend\OrdersDetailController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new OrdersDetailController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/orders')}}">Order</a> / {{ $subtitle->orders_id}} </h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">


                            <div class="portlet-title">
                                Order Detail
                            </div>

{{--test--}}
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tr>
                                            <th colspan='2'><h3><b><p class="caption-subject font-blue-sharp">Order Code {{ $subtitle->orders_id }}  </p></b></h3>
                                                <td colspan="2">Order by</td>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td><strong>Product Name</strong></td>
                                            <td><strong>Price</strong></td>
                                            <td><strong>Qty</strong></td>
                                            <td><strong>Ship</strong></td>
                                        </tr>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td>{!! Html::decode($field->product_name) !!}</td>
                                                    <td>{!! Html::decode($field->unit_price) !!}</td>
                                                    <td>{!! Html::decode($field->qty) !!}</td>
                                                    <td><input type="text" name="shipped" class="form-control" style="width: 50px;"></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='5' class='text-center'>No Result.</td></tr>
                                        @endif
                                        <tr>
                                            <td colspan="4" class="text-right"><button type="submit" class="btn green">Save</button></td>
                                        </tr>
                                        {{--<tr>--}}
                                            {{--<td class="table-bordered"></td>--}}
                                            {{--<td>--}}
                                                {{--Total Price--}}
                                            {{--</td>--}}
                                            {{--<td>{{ $subtitle->total }}</td>--}}

                                        {{--</tr>--}}

                                    </table>

                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection