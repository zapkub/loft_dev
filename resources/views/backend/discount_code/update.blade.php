<?php
use App\Http\Controllers\Backend\DiscountCodeController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new DiscountCodeController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-user" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Code</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="code" id="code" class="form-control" value="{{ $code }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Code Name</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="code_name" id="code_name" class="form-control" value="{{ $code_name }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Allot</label>
                                            <div class="col-md-4">
                                                <input type='number' min="0" name="allot" id="allot" class="form-control" value="{{ $allot }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Reserved</label>
                                            <div class="col-md-4">
                                                <input type='number' min="0" name="reserved" id="reserved" class="form-control" value="{{ $reserved }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Used</label>
                                            <div class="col-md-4">
                                                <input type='number' min="0" name="used" id="used" class="form-control" value="{{ $used }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Per Use</label>
                                            <div class="col-md-4">
                                                <input type='number' min="0" name="per_use" id="per_use" class="form-control" value="{{ $per_use }}">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Minimum Price</label>
                                            <div class="col-md-4">
                                                <input type='number' min="0" name="min_price" id="min_price" class="form-control" value="{{ $min_price }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Maximum Price</label>
                                            <div class="col-md-4">
                                                <input type='number' min="0" name="max_price" id="max_price" class="form-control" value="{{ $max_price }}">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Discount Type</label>
                                            <div class="col-md-4">

                                                <select name="discount_type" class="form-control select2-container form-control select2me">

                                                    <option value="fix" @if($status == "fix") selected @endif >Fix</option>
                                                    <option value="%"@if($status == "%") selected @endif >%</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Discount</label>
                                            <div class="col-md-4">
                                                <input type='number' min="0" name="discount" id="discount" class="form-control" value="{{ $discount }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Discount Max</label>
                                            <div class="col-md-4">
                                                <input type='number' min="0" name="discount_max" id="discount_max" class="form-control" value="{{ $discount_max }}">
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="control-label col-md-3">Start Show Date</label>
                                        <div class="col-md-4">
                                            <input type='text' name="show_date" id="show_date" class="form-control" value="{{ $show_date }}" data-date-format="yyyy-mm-dd hh:ii" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Start Use Date</label>
                                        <div class="col-md-4">
                                            <input type='text' name="start_date" id="start_date" class="form-control" value="{{ $start_date }}" data-date-format="yyyy-mm-dd hh:ii" readonly>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3">End Date</label>
                                        <div class="col-md-4">
                                            <input type='text' name="end_date" id="end_date" class="form-control" value="{{ $end_date }}" data-date-format="yyyy-mm-dd hh:ii" readonly>
                                        </div>
                                    </div>



                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status</label>
                                            <div class="col-md-4">

                                                <select name="status" class="form-control select2-container form-control select2me">

                                                    <option value="0" @if($status == 0) selected @endif >Close</option>
                                                    <option value="1"@if($status == 1) selected @endif >Open</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script>
        $(function() {
            $( "#show_date,#start_date,#end_date" ).datetimepicker();

        });
    </script>

@endsection
