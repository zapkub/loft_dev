<?php
use App\Http\Controllers\Backend\ContactUsController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ContactUsController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);


?>

@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-book" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">


                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">






                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Company Name[TH]</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="company_name_th" id="company_name_th" class="form-control" value="{{ $company_name_th }}">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Company Name[EN]</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="company_name_en" id="company_name_en" class="form-control" value="{{ $company_name_en }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address [TH]</label>
                                            <div class="col-md-9">
                                                <textarea name="address_th" id="address_th" class="form-control ckeditor">{{ $address_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address [EN]</label>
                                            <div class="col-md-9">
                                                <textarea name="address_en" id="address_en" class="form-control ckeditor">{{ $address_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mobile</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="mobile" id="mobile" class="form-control" value="{{ $mobile }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fax</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="fax" id="fax" class="form-control" value="{{ $fax }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="email" id="email" class="form-control" value="{{ $email }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>



                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>


@endsection
