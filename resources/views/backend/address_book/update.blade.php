<?php
use App\Http\Controllers\Backend\AddressBookController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new AddressBookController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-user" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Billing Shipping</label>
                                            <div class="col-md-4">

                                                <select name="billing_shipping" class="form-control select2-container form-control select2me">

                                                    <option value="0" @if($billing_shipping == 0) selected @endif >Shipping</option>
                                                    <option value="1"@if($billing_shipping == 1) selected @endif >Billing</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>






                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">User ID</label>
                                            <div class="col-md-4">
                                                <input type='text' name="user_id" id="user_id" class="form-control" value="{{ $user_id }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address</label>
                                            <div class="col-md-4">
                                                <input type='text' name="address_1" id="address_1" class="form-control" value="{{ $address_1 }}">
                                                <input type='text' name="address_2" id="address_2" class="form-control" value="{{ $address_2 }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tambon</label>
                                            <div class="col-md-4">
                                                <input type='text' name="tambon" id="tambon" class="form-control" value="{{ $tambon }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Amphur</label>
                                            <div class="col-md-4">
                                                <input type='text' name="amphur" id="amphur" class="form-control" value="{{ $amphur }}">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Province</label>
                                            <div class="col-md-4">
                                                <input type='text' name="province" id="province" class="form-control" value="{{ $province }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Zip</label>
                                            <div class="col-md-4">
                                                <input type='text' name="zip" id="zip" class="form-control" value="{{ $zip }}">
                                            </div>
                                        </div>
                                    </div>




                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>


@endsection
