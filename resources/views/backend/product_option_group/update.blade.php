<?php
use App\Http\Controllers\Backend\ProductOptiongroupController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductOptiongroupController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-user" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Option group</label>
                                            <div class="col-md-4">
                                                <select name="option_group_id" class="form-control select2-container form-control select2me" >
                                                    @foreach($option_group as $field2)
                                                        <option value="{{$field2->option_group_id}}"  @if($option_group_id == $field2->option_group_id) selected @endif >{{$field2->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Require</label>
                                            <div class="col-md-4">
                                                <select name="require" class="form-control">
                                                    <option value="0" @if($require == 0) selected @endif  >No</option>
                                                    <option value="1"  @if($require == 1) selected @endif>Yes</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>







                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="product_id" value="{{Input::get('product_id')}}">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>


@endsection
