<?php
use App\Http\Controllers\Backend\ProductController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-product" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs nav-justified" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#product-tap1" aria-controls="product-tap1" role="tab" data-toggle="tab">
                                                Data
                                            </a>
                                        </li>

                                       
                                        <li role="presentation">
                                            <a href="#product-tap3" aria-controls="product-tap3" role="tab" data-toggle="tab">
                                                Image
                                            </a>
                                        </li>
                                    </ul>

                                    {{--TAB CONTENT--}}

                                    <div class="tab-content tab-outside">

                                        <!-- tab 1 -->
                                        <div role="tabpanel" class="tab-pane fade in active" id="product-tap1">
                                            <div class="row">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Product Name [EN]</label>
                                                        <div class="col-md-4">
                                                            <input type="text" name="product_name" id="product_name" class="form-control" value="{{ $product_name }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Product Name [TH]</label>
                                                        <div class="col-md-4">
                                                            <input type="text" name="product_name_th" id="product_name_th" class="form-control" value="{{ $product_name_th }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="control-group ">
                                                    <div class="controls">
                                                        <div id="thumbnail" class="text-center">
                                                            @if($photo != '')
                                                                <img src="<?php echo URL::asset('uploads/product/'.$photo) ?>" id="img_path2" style="max-width:400px;">
                                                            @else
                                                                <img src="" id="img_path2" style="max-width:400px;">
                                                            @endif

                                                            <input type="hidden" name="photo" id="img_path" value="<?php echo  $photo?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Image</label>
                                                        <div class="col-md-4">
                                                            @if($photo != '')
                                                                <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                                <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                            @elseif ($photo == '')
                                                                <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                                <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>

                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Detail</label>
                                                        <div class="col-md-9">
                                                            <textarea name="detail" id="detail" class="form-control ckeditor">{{ $detail}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Retail Price</label>
                                                        <div class="col-md-4">
                                                            <input type="retail_price" min="0" step="0.01" name="retail_price" id="retail_price" class="form-control" value="{{ $retail_price }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Minimum Quantity</label>
                                                        <div class="col-md-4">
                                                            <input type="minimum_qty" min="1" step="1" name="minimum_qty" id="minimum_qty" class="form-control" value="{{ $minimum_qty }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Minimum Quantity</label>
                                                        <div class="col-md-4">
                                                            <input type="qty" min="0" step="1" name="qty" id="qty" class="form-control" value="{{ $qty }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Status</label>
                                                        <div class="col-md-4">
                                                            <select name="status" class="form-control select2-container select2me">
                                                                <option value="0" @if($status == 0) selected @endif >Disabled</option>
                                                                <option value="1"  @if($status == 1) selected @endif>Enabled</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Meta Title</label>
                                                        <div class="col-md-4">
                                                            <input type="text"  name="meta_title" id="meta_title" class="form-control" value="{{ $meta_title}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Meta Keyword</label>
                                                        <div class="col-md-4">
                                                            <input type="text"  name="meta_keyword" id="meta_keyword" class="form-control" value="{{ $meta_keyword}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Meta Description</label>
                                                        <div class="col-md-6">
                                                            <textarea name="meta_description" id="meta_description" class="form-control">{{ $meta_description}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Model</label>
                                                        <div class="col-md-4">
                                                            <input type="text"  name="model" id="model" class="form-control" value="{{ $model}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SKU</label>
                                                        <div class="col-md-4">
                                                            <input type="text"  name="sku" id="sku" class="form-control" value="{{ $sku}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">EAN</label>
                                                        <div class="col-md-4">
                                                            <input type="text"  name="ean" id="ean" class="form-control" value="{{ $ean}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Available</label>
                                                        <div class="col-md-4">
                                                            <p><input type='date' name="available_date" id="available_date" class="form-control" value="{{ $available_date }}" data-date-format="yyyy-mm-dd"></p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Dimension Length</label>
                                                        <div class="col-md-4">
                                                            <input type="number" min="0" step="0.01" name="dimension_l" id="dimension_l" class="form-control" value="{{ $dimension_l }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Dimension Width</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" step="0.01" name="dimension_w" id="dimension_w" class="form-control" value="{{ $dimension_w }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Dimension Height</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" step="0.01" name="dimension_h" id="dimension_h" class="form-control" value="{{ $dimension_h }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Dimension Class</label>
                                                        <div class="col-md-4">
                                                            <select name="dimension_class_id" class="form-control select2-container form-control select2me" >
                                                                @foreach($dimension as $field3)
                                                                    <option value="{{$field3->dimension_class_id}}" @if($dimension_class_id == $field3->dimension_class_id) selected @endif >{{$field3->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Weight</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" step="0.01" name="weight" id="weight" class="form-control" value="{{ $weight }}">
                                                            </div>
                                                        </div>
                                                </div>









                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Weight Class</label>
                                                        <div class="col-md-4">
                                                            <select name="weight_class_id" class="form-control select2-container form-control select2me" >
                                                                @foreach($weight1 as $field4)
                                                                    <option value="{{$field4->weight_class_id}}" @if($weight_class_id == $field4->weight_class_id) selected @endif >{{$field4->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Manufacturer</label>
                                                        <div class="col-md-4">
                                                            <select name="manufacturer_id" class="form-control select2-container form-control select2me" >
                                                                @foreach($manufacturer as $field5)
                                                                    <option value="{{$field5->manufacturer_id}}" @if($manufacturer_id == $field5->manufacturer_id) selected @endif >{{$field5->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Stock Status</label>
                                                            <div class="col-md-4">
                                                                <select name="stock_status_id" class="form-control select2-container form-control select2me" >
                                                                    @foreach($data2 as $field2)
                                                                        <option value="{{$field2->stock_status_id}}" @if($stock_status_id == $field2->stock_status_id) selected @endif >{{$field2->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                </div>

                                            </div>
                                        </div>

                                        <!-- tab 2 -->

                                        <!-- tab 3 -->
                                        <div role="tabpanel" class="tab-pane fade" id="product-tap3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    Tab 3
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    {{--END TAB CONTENT--}}



                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
@endsection
