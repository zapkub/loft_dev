<?php
use App\Http\Controllers\Backend\ProductController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal"  method="GET" >
                                <div class="form-group">

                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">

                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by Name</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <input type="hidden" name="stock_status_id" value="{{Input::get('stock_status_id')}}">

                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {!! Form::open(array('url' => $path , 'method' => 'GET' ,'id'=>'form-tag-product', 'class' => 'form-horizontal')) !!}

                                <div class="form-group">

                                    <label class="control-label col-md-1 ">Tag :</label>
                                    <div class="col-md-3">

                                        <div class="input-group">
                                        <input class="form-control" type="text" name="tag_name"  value="" placeholder="insert tag name" >
                                            <div class="input-group-btn">
                                                <button class="btn blue " type="submit" ></i>Set Tag</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center"><input type="checkbox" name="selectedAll" id="selectedAll"></th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="text-center">{!! $mainFn->sorting('Name','product_name',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Price','retail_price',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Status','status',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Stock Status','stock_status_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Product Option Group','product_option_group',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">
                                                <a href="{{ URL::to($path.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="span1"><div class="text-center">
                                                            <input type="checkbox" name="product_chk[]" id="product_chk[]" class="checkbox1" value="{{ $field->$pkField }}"></div>
                                                    </td>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    {!! Form::close() !!}
                                                    <td>{!! Html::decode($field->product_name) !!}</td>
                                                    <td class="text-right">{{number_format($field->retail_price,2)}} </td>

                                                    @if($field->status == 0)
                                                        <td><div class="text-center"><a id="status-{{$field->$pkField}}" class=" change-active fa fa-close text-red" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                    @else
                                                        <td><div class="text-center"><a id="status-{{$field->$pkField}}" class=" change-active fa fa-check text-green" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                    @endif

                                                    <td>{{$field->StockStatus->name}}</td>

                                                    <td class="text-center"><a href="{{URL::to('_admin/product_option_group?'.$pkField.'='.$field->$pkField)}}" class="btn btn-xs btn-circle yellow"><i class="fa fa-ellipsis-h"></i></a></td>

                                                    <td class="text-center">
                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                        <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                            <input name="_method" type="hidden" value="delete">
                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                            <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='5' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

    <script>
        $(document).ready(function() {
            $('#selectedAll').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox1').attr('checked', 'checked');
                    $('.checkbox1').parent().addClass('checked');
                }else{
                    $('.checkbox1').removeAttr('checked');
                    $('.checkbox1').parent().removeClass('checked');
                }
            });

        });

    </script>

@endsection
@section('js')
    <script src="{{URL::asset('js/change-status.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/tag-product.js')}}"></script>

@endsection