<?php
use App\Http\Controllers\Backend\CategoryController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CategoryController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-user" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Category Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="category_name" id="category_name" class="form-control" value="{{ $category_name }}" >
                                            </div>
                                        </div>
                                    </div>

                                    {{--<div class="form-body">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label col-md-3">Parent Category</label>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<input type='text' name="parent_category_id" id="parent_category_id" class="form-control" value="{{ $parent_category_id}}" >--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Parent Category</label>
                                            <div class="col-md-4">
                                                <select name="parent_category_id" class="form-control select2-container form-control select2me" >
                                                    <option value="0">/</option>
                                                    @foreach($category as $field1)
                                                        <option value="{{$field1->category_id}}" @if($category_id == $field1->category_id) selected @endif >{{$field1->category_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>





                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Meta Title</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="meta_title" id="meta_title" class="form-control" value="{{ $meta_title }}" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Meta Keyword</label>
                                            <div class="col-md-4">
                                                <input type='text' name="meta_keyword" id="meta_keyword" class="form-control" value="{{ $meta_keyword }}" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Meta Description</label>
                                            <div class="col-md-4">
                                                <input type='text' name="meta_description" id="meta_description" class="form-control" value="{{ $meta_description }}" >
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>


@endsection
