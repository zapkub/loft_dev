<?php
use App\Http\Controllers\Backend\UserController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new UserController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;
$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by Name</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr >
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="text-center">{!! $mainFn->sorting('First Name','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Last Name','lastname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Email','email',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Mobile','mobile',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Status','status',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">
                                                <a href="{{ URL::to($path.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{!! Html::decode($field->firstname) !!}</td>
                                                    <td>{!! Html::decode($field->lastname) !!}</td>
                                                    <td>{!! Html::decode($field->email) !!}</td>
                                                    <td>{!! Html::decode($field->mobile) !!}</td>
                                                    @if($field->status == 0)
                                                        <td><div class="text-center"><a id="status-{{$field->$pkField}}" class=" change-active fa fa-close text-red" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                    @else
                                                        <td><div class="text-center"><a id="status-{{$field->$pkField}}" class=" change-active fa fa-check text-green" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                    @endif

                                                    <td class="text-center">
                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                        <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                            <input name="_method" type="hidden" value="delete">
                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                            <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='5' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
@section('js')
    <script src="{{URL::asset('js/change-status.js')}}"></script>
@endsection