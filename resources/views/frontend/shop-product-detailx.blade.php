@extends('layout-frontend')
    @section('content')
        <div id="wrapper">
            <div class="container">

                <form action="{{ URL::to('api/shop/cart/add') }}" class="form-horizontal" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="product_id" value="{{ $product->product_id }}">

                    <h3>{{ $product->product_name }}</h3>
                    <hr>
                    <p>{{ $product->detail }}</p>
                    <p>Price :
                        @if($product->price != null)
                            {{ $product->price }}
                        @else
                            {{ $product->retail_price }}
                        @endif
                         บาท
                    </p>
                    @if($product->point != 0)
                        <p>Point : {{ $product->point }} Point(s)</p>
                    @endif


                    <br>

                    <?php
                    $optionGroup = $product->product_option_group;
                    $countGroup = count($optionGroup);
                    ?>
                    @if($countGroup > 0)
                        <h4>Select Option</h4>

                        @foreach($optionGroup as $groups)
                            <?php
                                $group_type = $groups->group_type;
                                $group_name = strtolower($groups->group_name);
                                $a_group_option = $groups->option
                            ?>
                            <h5>{{ ucfirst($group_name) }}</h5>
                            @if($group_type == 'radio')
                                @foreach($a_group_option as $option)
                                    <?php $optionTxt = $option->option_name; ?>
                                    @if($option->price != 0)
                                        <?php $optionTxt .= " [ +".$option->price." ฿ ]"; ?>
                                    @endif
                                    @if($option->point != 0)
                                        <?php $optionTxt .= " [ +".$option->point." point(s) ]"; ?>
                                    @endif
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="{{ $group_name }}" id="" value="{{ $option->option_id }}">
                                            {{ $optionTxt }}
                                        </label>
                                    </div>
                                @endforeach
                            @elseif($group_type == 'checkbox')
                                @foreach($a_group_option as $option)
                                    <?php $optionTxt = $option->option_name; ?>
                                    @if($option->price != 0)
                                        <?php $optionTxt .= " [ +".$option->price." ฿ ]"; ?>
                                    @endif
                                    @if($option->point != 0)
                                        <?php $optionTxt .= " [ +".$option->point." point(s) ]"; ?>
                                    @endif
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="{{ $group_name }}[]" value="{{ $option->option_id }}">
                                            {{ $optionTxt }}
                                        </label>
                                    </div>
                                @endforeach

                            @elseif($group_type == 'selectbox')
                                <select name="{{ $group_name }}" class="form-control">
                                    @foreach($a_group_option as $option)
                                        <?php $optionTxt = $option->option_name; ?>
                                        @if($option->price != 0)
                                            <?php $optionTxt .= " [ +".$option->price." ฿ ]"; ?>
                                        @endif
                                        @if($option->point != 0)
                                            <?php $optionTxt .= " [ +".$option->point." point(s) ]"; ?>
                                        @endif
                                        <option value="{{ $option->option_id }}">{{ $optionTxt }}</option>
                                    @endforeach
                                </select>
                            @endif
                        @endforeach

                    @endif


                    <br><br>

                    <button class="btn btn-primary" type="submit">Add to cart</button>
                    {{--<a href="#" class="btn btn-primary">Add to cart</a>--}}

                    <hr>
                    <a href="{{ URL::to('api/shop/product') }}" class="btn btn-info">Back</a>
                </form>
            </div>
        </div>
    @endsection