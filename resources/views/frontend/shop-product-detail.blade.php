@extends('layout-frontend')
    @section('content')
        <div id="wrapper">
            <div class="container">

                <form action="{{ URL::to('api/shop/cart/add') }}" class="form-horizontal" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="product_id" value="{{ $product->product_id }}">

                    <h3>{{ $product->product_name }}</h3>
                    <hr>
                    <p>{{ $product->detail }}</p>
                    <p>Price :
                        @if($product->price != null)
                            {{ $product->price }}
                        @else
                            {{ $product->retail_price }}
                        @endif
                         บาท
                    </p>
                    @if($product->point != 0)
                        <p>Point : {{ $product->point }} Point(s)</p>
                    @endif
                    <br>
                    <?php
                    $optionGroup = $product->product_option_group;
                    $countGroup = count($optionGroup);
                    ?>
                    @if($countGroup > 0)
                        <?php
                        $optionGroup = $product->product_option_group;
                        $groupName = $optionGroup->group_name;
                        $a_product_option = $optionGroup->option;
                        ?>
                        <h5>{{ $groupName }}</h5>
                        <div class="row">
                            <div class="col-sm-4">
                                <select class="form-control" name="product_option_id" required>
                                    <option value="">Please select</option>
                                    @foreach($a_product_option as $option)
                                        <?php $optionTxt = $option->option_name; ?>
                                        @if($option->price != 0)
                                            <?php $optionTxt .= " [ +".$option->price." ฿. ]"; ?>
                                        @endif
                                        <option value="{{ $option->product_option_id }}" @if($option->qty < 1) {{ 'disabled' }} @endif>{{ $optionTxt }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    @endif
                    <br><br>

                    <button class="btn btn-primary" type="submit">Add to cart</button>

                    <hr>
                    <a href="{{ URL::to('api/shop/product') }}" class="btn btn-info">Back</a>
                </form>
            </div>
        </div>
    @endsection