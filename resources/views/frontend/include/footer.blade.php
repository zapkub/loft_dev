<footer>
    <div class="container">
        <div class="row">
        	<div class="col-sm-12">
                        <span class="txt-black">Subscribe to our enewsletter and get special promotions</span> <input type="text" style="margin-top:23px; margin-bottom:10px"> <button type="button" class="btn btn-primary button-black" data-toggle="button" aria-pressed="false" autocomplete="off">
  SUBSCRIBE
</button>
            </div>
            <div class="section-1 col-xs-12">
                    <div class="col-sm-2">
                    	<div class="row">
                            <div class="col-sm-12"><img src="{{ URL::asset('images/logo-footer.gif')}}" alt="" class="img-responsive"/></div>
                        </div>
                    </div>
                    <div class="col-sm-2 footer-account">
                    	<div class="col-xs-12">
                            <span class="footer-header">ABOUT US</span><br><a href="#">Corporate Info</a><br><a href="#">Brand History</a><br><a href="#">Privacy Policy</a><br><a href="#">Contact Us</a><br><a href="#">Branches</a><br><a href="#">Sitemap</a>
                            
                            </div>
                    </div>
                    <div class="col-sm-2 footer-account">
                    	<div class="col-xs-12">
                    	<span class="footer-header">FAQ</span><br><a href="#">How To Order</a><br><a href="#">Payment Method</a><br><a href="#">Delivery</a><br><a href="#">Return Policy</a></div>
                    </div>
                    <div class="col-sm-2 footer-account">
                    	<div class="col-xs-12">
                    	<span class="footer-header">SERVICES</span><br><a href="#">Tracking Order</a><br><a href="#">Point Check</a></div>
                    </div>
                    <div class="col-sm-3 footer-account">
                    	<div class="col-xs-12">
                    	<span class="footer-header">PAYMENT</span><br><span class="footer-header">DELIVERY SERVICES</span></div>
                    </div>
                        
            </div>
            <div class="section-2 col-xs-12">
Copyright 2016 LOFT THAILAND. All right reserved<br>E-commerce registration number:1000100010 
            </div>
        </div>
    </div>
</footer>