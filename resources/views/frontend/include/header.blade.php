<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-toggle" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::to('/')}}"><img src="{{ URL::asset('images/logo.gif')}}" alt="" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="menu-toggle">
            <ul class="nav navbar-nav navbar-right hidden-sm">
                <li class="nav-a-text"><a href="{{URL::to('/')}}">Home</a></li>
                <li class="nav-a-text"><a href="{{URL::to('product')}}">Products</a></li>
                <li class="nav-a-text"><a href="{{URL::to('promotion')}}">Promotions</a></li>
                <li class="nav-a-text"><a href="#">News & Activities</a></li>
                <!--<li class="nav-a-text"><a href="#">Blog</a></li>-->
                <li class="nav-a-text"><a href="#">Member</a></li>
                <li><a href="#"><img src="{{ URL::asset('images/nav-icon-01.gif')}}" alt=""/></a></li>
                <li><a href="#"><img src="{{ URL::asset('images/nav-icon-02.gif')}}" alt=""/></a></li>
                <li><a href="#"><img src="{{ URL::asset('images/nav-icon-03.gif')}}" alt=""/></a></li>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu 3 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Sub Menu 1</a></li>
                        <li><a href="#">Sub Menu 2</a></li>
                        <li><a href="#">Sub Menu 3</a></li>
                        <li><a href="#">Sub Menu 4</a></li>
                    </ul>
                </li>
-->            </ul>
				<ul class="nav nav2 navbar-nav navbar-right hidden-md hidden-lg hidden-xs">
				<li><a href="#"><img src="{{ URL::asset('images/nav-icon-01.gif')}}" alt=""/></a></li>
                <li><a href="#"><img src="{{ URL::asset('images/nav-icon-02.gif')}}" alt=""/></a></li>
                <li><a href="#"><img src="{{ URL::asset('images/nav-icon-03.gif')}}" alt=""/></a></li>
                </ul>
                <ul class="nav nav2 navbar-nav navbar-right hidden-md hidden-lg hidden-xs">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">News & Activities</a></li>
                <li><a href="#">Member</a></li>
                <li><a href="#">Contact us</a></li>
                
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu 3 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Sub Menu 1</a></li>
                        <li><a href="#">Sub Menu 2</a></li>
                        <li><a href="#">Sub Menu 3</a></li>
                        <li><a href="#">Sub Menu 4</a></li>
                    </ul>
                </li>
-->            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>