@extends('layout-frontend')
    @section('content')
    <script>
	$('#myTabs a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
	})
	$('#myTabs a[href="#profile"]').tab('show') // Select tab by name
	$('#myTabs a:first').tab('show') // Select first tab
	$('#myTabs a:last').tab('show') // Select last tab
	$('#myTabs li:eq(2) a').tab('show') // Select third tab (0-indexed)
	</script>
        <div id="wrapper">
            <div class="container-fluid container-product">
            	<div class="container">
				<div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-01.jpg')}}" alt="" class="img-responsive"/></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-02.jpg')}}" alt="" class="img-responsive"/></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-03.jpg')}}" alt="" class="img-responsive"/></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-04.jpg')}}" alt="" class="img-responsive"/></div>
                </div>
            </div>
            <div class="container">
				<div class="col-xs-12 container-product-pagelink">Home<span class="txt-black"> > Light</span><hr class="hr-footer"></div>
            </div>
            
            <div class="container text-center">
				<div class="col-sm-3 container-product-detail-left ">
                	<div class="col-1sm-12 left-nav text-left">
                    	<div class="name">Product Categories</div>
                        <div class="cat">Furniter</div>
                		<div class="sub-cat"><span class="glyphicon glyphicon-arrow-right"></span> Kitchen LightingBathroom</div>
                        <div class="sub-cat"><span class="glyphicon glyphicon-arrow-right"></span> LightingDining Room</div>
                        <div class="sub-cat"><span class="glyphicon glyphicon-arrow-right"></span> LightingBedRoom</div>
                        <div class="cat">Home Improvement</div>
                		<div class="sub-cat"><span class="glyphicon glyphicon-arrow-right"></span> Bathroom Fixtures</div>
                        <div class="sub-cat"><span class="glyphicon glyphicon-arrow-right"></span> Flooring</div>
                        <div class="sub-cat"><span class="glyphicon glyphicon-arrow-right"></span> Large Appliances</div>
                        <div class="sub-cat"><span class="glyphicon glyphicon-arrow-right"></span> Heating & Cooling</div>
                    </div>
                    <div class="col-sm-12 left-nav text-left">
                    	<div class="col-sm-12 name2 row">Top Sellers</div>
                        <div class="row topsell">
                 		<div class="col-sm-5"><img src="{{ URL::asset('images/product-detail-left-top-seller.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-7 row">
                        	<div class="name3">Uptown 6-light Chandelier</div>
                            <div class="price">$12.49</div>
                        </div>
                        </div>
                        <div class="row topsell">
                 		<div class="col-sm-5"><img src="{{ URL::asset('images/product-detail-left-top-seller02.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-7 row">
                        	<div class="name3">Uptown 6-light Chandelier</div>
                            <div class="price">$12.49</div>
                        </div>
                        </div>
                        <div class="row topsell">
                 		<div class="col-sm-5"><img src="{{ URL::asset('images/product-detail-left-top-seller03.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-7 row">
                        	<div class="name3">Uptown 6-light Chandelier</div>
                            <div class="price">$12.49</div>
                        </div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="col-sm-12 row">
                        <img src="{{ URL::asset('images/product-detail-left-banner.jpg')}}" alt="" class="img-responsive"/>
                    </div>
                </div>
                <div class="col-sm-9 container-product-detail">
                	<div class="col-sm-12">
                    	<div class="row ">
                    	<div class="col-sm-6">
                        	<div class="row"><img src="{{ URL::asset('images/product-detail-pic.jpg')}}" alt="" class="img-responsive"/></div>
                            <div class="row picthumb">
                            	<div class="col-sm-3"><div class="row"><img src="{{ URL::asset('images/product-detail-pic-thumb.jpg')}}" alt="" class="img-responsive"/></div></div>
                                <div class="col-sm-3"><div class="row"><img src="{{ URL::asset('images/product-detail-pic-thumb02.jpg')}}" alt="" class="img-responsive"/></div></div>
                                <div class="col-sm-3"><div class="row"><img src="{{ URL::asset('images/product-detail-pic-thumb03.jpg')}}" alt="" class="img-responsive"/></div></div>
                                <div class="col-sm-3"><div class="row"><img src="{{ URL::asset('images/product-detail-pic-thumb04.jpg')}}" alt="" class="img-responsive"/></div></div>
                            </div>
                            <div class="row social">
                            <img src="{{ URL::asset('images/nav-icon-fb-s.gif')}}" alt=""/> <img src="{{ URL::asset('images/nav-icon-tw-s.gif')}}" alt=""/> <img src="{{ URL::asset('images/nav-icon-pi-s.gif')}}" alt=""/> <img src="{{ URL::asset('images/nav-icon-yt-s.gif')}}" alt=""/>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 text-left">
                        	<div class="col-sm-12">
                   	  <div class="name">Product Name</div>
                      <div class="star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                        <div class="detail">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. tempor incididunt ut labore et dolore magna</div>
                        <div class="price">$80 <span class="price-cut">$95.30</span> </div>
                        <div class="qty"><input type="text"></div>
                        <div><a class="btn btn-default btn-yellow" href="#" role="button">ADD TO CART</a></div>
						<div class="heart"><span class="glyphicon glyphicon-heart-empty"></span></div>
                    </div>
                    </div>
                        </div>
                    </div>
                    <div class="col-sm-12 descript text-left">
                    	<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
    <li role="presentation"><a href="#additional" aria-controls="additional" role="tab" data-toggle="tab">Additional Information</a></li>
    <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Reviews</a></li>
    <li role="presentation"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab">Tags</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content tab-content-pad">
    <div role="tabpanel" class="tab-pane active" id="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</div>
    <div role="tabpanel" class="tab-pane" id="additional">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. </div>
    <div role="tabpanel" class="tab-pane" id="reviews">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. </div>
    <div role="tabpanel" class="tab-pane" id="tags">Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. </div>
  </div>

</div>
                    </div>
                	<div class="col-sm-12">
                    	<div class="row">

                    <!-- Nav tabs -->
  <ul class="nav nav-tabs nav-center" role="tablist">
    <li role="presentation" class="active"><a href="#related" aria-controls="related" role="tab" data-toggle="tab">Related Products</a></li>
    <li role="presentation"><a href="#upsell" aria-controls="upsell" role="tab" data-toggle="tab">Up-sell Products</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content row">
    <div role="tabpanel" class="tab-pane active container-product-page" id="related">
    	<div class="col-sm-4"><img src="{{ URL::asset('images/Product-Cat-A_01.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-4"><img src="{{ URL::asset('images/Product-Cat-A_02.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-4"><img src="{{ URL::asset('images/Product-Cat-A_03.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
    </div>
    <div role="tabpanel" class="tab-pane container-product-page" id="upsell">
    	<div class="col-sm-4"><img src="{{ URL::asset('images/Product-Cat-A_01.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-4"><img src="{{ URL::asset('images/Product-Cat-A_02.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-4"><img src="{{ URL::asset('images/Product-Cat-A_03.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-4"><img src="{{ URL::asset('images/Product-Cat-A_01.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-4"><img src="{{ URL::asset('images/Product-Cat-A_02.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-4"><img src="{{ URL::asset('images/Product-Cat-A_03.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
    </div>

  </div>
                	
               
                </div>
             	</div>
                </div>
            </div>
            
        </div>
    @endsection