@extends('layout-frontend')
    @section('content')
        <div id="wrapper">
            <div class="container-fluid home-banner">
				<div class="container">
		    		<img src="{{ URL::asset('images/index-banner.jpg')}}" alt="" class="img-responsive"/>
    			</div>
            </div>
            <div class="container-home">
            	<div class="container">
                    <div class="col-sm-6"><img src="{{ URL::asset('images/index-banner-con-01.jpg')}}" alt="" class="img-responsive" vspace="18"/></div>
                    <div class="col-sm-6"><img src="{{ URL::asset('images/index-banner-con-02.jpg')}}" alt="" class="img-responsive" vspace="18"/></div>
                    <div class="col-sm-6"><img src="{{ URL::asset('images/index-banner-con-03.jpg')}}" alt="" class="img-responsive" vspace="18"/></div>
                    <div class="col-sm-6"><img src="{{ URL::asset('images/index-banner-con-04.jpg')}}" alt="" class="img-responsive" vspace="18"/></div>
                </div>
            </div>
            <div class="container-fluid">
            	<div class="row container-arrivals">
                    <div class="col-sm-4 col-md-3">
                    	<div class="row"><img src="{{ URL::asset('images/new-arrivals-txt.gif')}}" alt="" class="img-responsive"/></div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                    	<div class="row"><img src="{{ URL::asset('images/new-arrival-01.jpg')}}" alt="" class="img-responsive"/></div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                    	<div class="row"><img src="{{ URL::asset('images/new-arrival-02.jpg')}}" alt="" class="img-responsive"/></div>
                    </div>
                    <div class="col-md-3 hidden-sm hidden-xs">
                    	<div class="row"><img src="{{ URL::asset('images/new-arrival-03.jpg')}}" alt="" class="img-responsive"/></div>
                    </div>
                </div>
            </div>
            <div class="container">
                    <div class="col-sm-12 container-regist"><img src="{{ URL::asset('images/regis.jpg')}}" alt="" class="img-responsive"/></div>
            </div>
            <div class="container">
                    <div class="col-sm-12"><img src="{{ URL::asset('images/recommend-txt.jpg')}}" alt="" class="img-responsive"/></div>
            </div>
            <div class="container text-center container-recommend">
                    <div class="col-sm-4"><img src="{{ URL::asset('images/sample-product-01.jpg')}}" alt="" class="img-responsive"/>Product Name<br>2,250</div>
                    <div class="col-sm-4"><img src="{{ URL::asset('images/sample-product-02.jpg')}}" alt="" class="img-responsive"/>Product Name<br>2,250</div>
                    <div class="col-sm-4"><img src="{{ URL::asset('images/sample-product-03.jpg')}}" alt="" class="img-responsive"/>Product Name<br>2,250</div>
                    <div class="col-sm-4"><img src="{{ URL::asset('images/sample-product-04.jpg')}}" alt="" class="img-responsive"/>Product Name<br>2,250</div>
                    <div class="col-sm-4"><img src="{{ URL::asset('images/sample-product-05.jpg')}}" alt="" class="img-responsive"/>Product Name<br>2,250</div>
                    <div class="col-sm-4"><img src="{{ URL::asset('images/sample-product-06.jpg')}}" alt="" class="img-responsive"/>Product Name<br>2,250</div>
            </div>
        </div>
    @endsection