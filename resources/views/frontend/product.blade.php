@extends('layout-frontend')
    @section('content')
        <div id="wrapper">
            <div class="container-fluid container-product">
            	<div class="container">
				<div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-01.jpg')}}" alt="" class="img-responsive"/></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-02.jpg')}}" alt="" class="img-responsive"/></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-03.jpg')}}" alt="" class="img-responsive"/></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-04.jpg')}}" alt="" class="img-responsive"/></div>
                </div>
            </div>
            <div class="container">
				<div class="col-xs-12 container-product-pagelink">Home<span class="txt-black"> > Light</span><hr class="hr-footer"></div>
            </div>
            <div class="container">
            	<div class="col-sm-6">
                <a class="btn btn-default button-black" href="{{URL::to('product')}}" role="button"><span class="glyphicon glyphicon-th-large"></span> GRID</a>&nbsp;<a class="btn btn-default button-black" href="{{URL::to('product_list')}}" role="button"><span class="glyphicon glyphicon-th-list"></span> LIST</a>
  				</div>
                <div class="col-sm-6 text-right">
                                <nav style="display:inline">
  <ul class="pagination pagination-sm">
    <!--<li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>-->
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true"><span class="glyphicon glyphicon-menu-right"></span></span>
      </a>
    </li>
  </ul>
</nav>
                </div>
            </div>
            <div class="container container-product-page text-center">
				<div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_01.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_02.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_03.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_04.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_05.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_06.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_07.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_08.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_09.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_10.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_11.jpg')}}" alt="" class="img-responsive"/>Product Name<br><span class="price">2,250</span></div>
                
            </div>
            <div class="container">
            	<div class="col-sm-12"><hr class="hr-product-footer"></div>
            	<div class="col-sm-6">
				<a class="btn btn-default button-black" href="{{URL::to('product')}}" role="button"><span class="glyphicon glyphicon-th-large"></span> GRID</a>
				&nbsp;<a class="btn btn-default button-black" href="{{URL::to('product_list')}}" role="button"><span class="glyphicon glyphicon-th-list"></span> LIST</a>
  				</div>
                <div class="col-sm-6 text-right">
                                <nav style="display:inline">
  <ul class="pagination pagination-sm">
    <!--<li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>-->
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true"><span class="glyphicon glyphicon-menu-right"></span></span>
      </a>
    </li>
  </ul>
</nav>
                </div>
            </div>
        </div>
    @endsection