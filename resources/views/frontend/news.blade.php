@extends('layout-frontend')
    @section('content')
        <div id="wrapper">
        	<div class="container about-banner">
				<div class="col-xs-12">Home<span class="txt-black"> > News & Activities</span><hr class="hr-footer"></div>
            </div>
			<div class="container container-news">
            	<div class="col-sm-9">
                		<div class="row news-box">
                    	<div class="col-sm-5"><img src="{{ URL::asset('images/News&Activities_01_06.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-7">
                        	<div class="name">Living Room Furniture</div>
                            <div class="post">Post by: <span class="post-name">7uptheme</span> - on jan 18, 2016</div>
                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            <div class="read"><span class="glyphicon glyphicon-arrow-right"></span> Read more</div>
                        </div>
                        <div class="row news-box">
                    	<div class="col-sm-5"><img src="{{ URL::asset('images/News&Activities_01_09.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-7">
                        	<div class="name"><span class="glyphicon glyphicon-film"></span> Living Room Furniture</div>
                            <div class="post">Post by: <span class="post-name">7uptheme</span> - on jan 18, 2016</div>
                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            <div class="read"><span class="glyphicon glyphicon-arrow-right"></span> Read more</div>
                        </div>
                        </div>
                        <div class="row news-box">
                    	<div class="col-sm-5"><img src="{{ URL::asset('images/News&Activities_01_17.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-7">
                        	<div class="name"><span class="glyphicon glyphicon-picture"></span> Living Room Furniture</div>
                            <div class="post">Post by: <span class="post-name">7uptheme</span> - on jan 18, 2016</div>
                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            <div class="read"><span class="glyphicon glyphicon-arrow-right"></span> Read more</div>
                        </div>
                        </div>
                        <div class="row news-box">
                    	<div class="col-sm-5"><img src="{{ URL::asset('images/News&Activities_01_22.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-7">
                        	<div class="name"><span class="glyphicon glyphicon-book"></span> Living Room Furniture</div>
                            <div class="post">Post by: <span class="post-name">7uptheme</span> - on jan 18, 2016</div>
                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            <div class="read"><span class="glyphicon glyphicon-arrow-right"></span> Read more</div>
                        </div>
                        </div>
                        <div class="row news-box">
                    	<div class="col-sm-5"><img src="{{ URL::asset('images/News&Activities_01_29.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-7">
                        	<div class="name"><span class="glyphicon glyphicon-star-empty"></span> Living Room Furniture</div>
                            <div class="post">Post by: <span class="post-name">7uptheme</span> - on jan 18, 2016</div>
                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            <div class="read"><span class="glyphicon glyphicon-arrow-right"></span> Read more</div>
                        </div>
                        </div>
                        <div class="row news-box">
                    	<div class="col-sm-5"><img src="{{ URL::asset('images/News&Activities_01_31.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-7">
                        	<div class="name"><span class="glyphicon glyphicon-time"></span> Living Room Furniture</div>
                            <div class="post">Post by: <span class="post-name">7uptheme</span> - on jan 18, 2016</div>
                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            <div class="read"><span class="glyphicon glyphicon-arrow-right"></span> Read more</div>
                        </div>
                        </div>
                </div>
                </div>
                <div class="col-sm-3 container-product-detail-left">
                	<div class="col-sm-12 right-nav text-left">
                    	<div class="col-sm-12">
                        	<div class="row"><img src="{{ URL::asset('images/News&Activities_01_03.jpg')}}" alt="" class="img-responsive"/></div>
                        </div>
                        <div class="row">
                        <div class="col-sm-6 topsell">
                        	<img src="{{ URL::asset('images/News&Activities_01_11.jpg')}}" alt="" class="img-responsive"/>
                        </div>
                        <div class="col-sm-6 topsell">
                        	<img src="{{ URL::asset('images/News&Activities_01_13.jpg')}}" alt="" class="img-responsive"/>
                        </div>
                        </div>
                        <div class="cat2">Categories</div>
                		<div class="sub-cat2"><span class="glyphicon glyphicon-plus"></span> Kitchen LightingBathroom</div>
                        <div class="sub-cat2"><span class="glyphicon glyphicon-plus"></span> LightingDining Room</div>
                        <div class="sub-cat2"><span class="glyphicon glyphicon-plus"></span> LightingBedRoom</div>
                    	<div class="cat2">Recent Posts</div>
                        <div class="row topsell">
                 		<div class="col-sm-4"><img src="{{ URL::asset('images/News&Activities_01_20.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-8 row">
                        	<div class="name4">Lorem ipsum dolor sit</div>
                            <div class="date">August 10, 2016</div>
                        </div>
                        </div>
                        <div class="row topsell">
                 		<div class="col-sm-4"><img src="{{ URL::asset('images/News&Activities_01_24.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-8 row">
                        	<div class="name4">Consectetur adipisicing elit</div>
                            <div class="date">August 10, 2016</div>
                        </div>
                        </div>
                        <div class="row topsell">
                 		<div class="col-sm-4"><img src="{{ URL::asset('images/News&Activities_01_26.jpg')}}" alt="" class="img-responsive"/></div>
                        <div class="col-sm-8 row">
                        	<div class="name4">sed do eiusmod tempor</div>
                            <div class="date">August 10, 2016</div>
                        </div>
                        </div>
                        <div class="cat2">Tags</div>
                		<div class="col-sm-12 row"><a class="btn btn-default tag" href="#" role="button">Decor</a><a class="btn btn-default tag" href="#" role="button">Furniture</a><a class="btn btn-default tag" href="#" role="button">Lighting</a><a class="btn btn-default tag" href="#" role="button">Bed & Bath</a><a class="btn btn-default tag" href="#" role="button">Chair</a><a class="btn btn-default tag" href="#" role="button">Table</a><a class="btn btn-default tag" href="#" role="button">Wood</a><a class="btn btn-default tag" href="#" role="button">Responsive</a></div>
                    </div>
                </div>

            </div>
        </div>
    @endsection