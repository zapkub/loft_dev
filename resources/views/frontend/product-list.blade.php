@extends('layout-frontend')
    @section('content')
        <div id="wrapper">
            <div class="container-fluid container-product">
            	<div class="container">
				<div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-01.jpg')}}" alt="" class="img-responsive"/></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-02.jpg')}}" alt="" class="img-responsive"/></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-03.jpg')}}" alt="" class="img-responsive"/></div>
                <div class="col-sm-3"><img src="{{ URL::asset('images/product-page-sec1-04.jpg')}}" alt="" class="img-responsive"/></div>
                </div>
            </div>
            <div class="container">
				<div class="col-xs-12 container-product-pagelink">Home<span class="txt-black"> > Light</span><hr class="hr-footer"></div>
            </div>
            <div class="container">
            	<div class="col-sm-6">
				<a class="btn btn-default button-black" href="{{URL::to('product')}}" role="button"><span class="glyphicon glyphicon-th-large"></span> GRID</a>
				&nbsp;<a class="btn btn-default button-black" href="{{URL::to('product_list')}}" role="button"><span class="glyphicon glyphicon-th-list"></span> LIST</a>
  				</div>
                <div class="col-sm-6 text-right">
                                <nav style="display:inline">
  <ul class="pagination pagination-sm">
    <!--<li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>-->
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true"><span class="glyphicon glyphicon-menu-right"></span></span>
      </a>
    </li>
  </ul>
</nav>
                </div>
            </div>
            <div class="container container-product-page container-product-detail">
            	<div class="col-sm-12">
                <!--- item ---->
                	<div class="col-sm-12">
                	<div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_01.jpg')}}" alt="" class="img-responsive"/></div>
                    <div class="col-sm-6 left">
                   	  <div class="name">Product Name</div>
                      <div class="star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                        <div class="detail">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. tempor incididunt ut labore et dolore magna</div>
                        <div><a class="btn btn-default btn-white " href="#" role="button">More Detail</a></div>
                      </div>
                    	<div class="col-sm-3 right">
                    		<div class="price">$80 <span class="price-cut">$95.30</span> </div>
                            <div class="heart"><span class="glyphicon glyphicon-heart-empty"></span></div>
                            <div class="qty"><input type="text"></div>
                            <div><a class="btn btn-default btn-yellow" href="#" role="button">ADD TO CART</a></div>
                    	</div>
                    </div>
                    <!--- item ---->
                	<div class="col-sm-12">
                	<div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_02.jpg')}}" alt="" class="img-responsive"/></div>
                    <div class="col-sm-6">
                   	  <div class="name">Product Name</div>
                      <div class="star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                        <div class="detail">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. tempor incididunt ut labore et dolore magna</div>
                        <div><a class="btn btn-default btn-white " href="#" role="button">More Detail</a></div>
                      </div>
                    	<div class="col-sm-3 right">
                    		<div class="price">$80 <span class="price-cut">$95.30</span> </div>
                            <div class="heart"><span class="glyphicon glyphicon-heart-empty"></span></div>
                            <div class="qty"><input type="text"></div>
                            <div><a class="btn btn-default btn-yellow" href="#" role="button">ADD TO CART</a></div>
                    	</div>
                    </div>
                    <!--- item ---->
                	<div class="col-sm-12">
                	<div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_03.jpg')}}" alt="" class="img-responsive"/></div>
                    <div class="col-sm-6">
                   	  <div class="name">Product Name</div>
                      <div class="star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                        <div class="detail">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. tempor incididunt ut labore et dolore magna</div>
                        <div><a class="btn btn-default btn-white " href="#" role="button">More Detail</a></div>
                      </div>
                    	<div class="col-sm-3 right">
                    		<div class="price">$80 <span class="price-cut">$95.30</span> </div>
                            <div class="heart"><span class="glyphicon glyphicon-heart-empty"></span></div>
                            <div class="qty"><input type="text"></div>
                            <div><a class="btn btn-default btn-yellow" href="#" role="button">ADD TO CART</a></div>
                    	</div>
                    </div>
                    <!--- item ---->
                	<div class="col-sm-12">
                	<div class="col-sm-3"><img src="{{ URL::asset('images/Product-Cat-A_04.jpg')}}" alt="" class="img-responsive"/></div>
                    <div class="col-sm-6">
                   	  <div class="name">Product Name</div>
                      <div class="star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                        <div class="detail">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. tempor incididunt ut labore et dolore magna</div>
                        <div><a class="btn btn-default " href="#" role="button">More Detail</a></div>
                      </div>
                    	<div class="col-sm-3 right">
                    		<div class="price">$80 <span class="price-cut">$95.30</span> </div>
                            <div class="heart"><span class="glyphicon glyphicon-heart-empty"></span></div>
                            <div class="qty"><input type="text"></div>
                            <div><a class="btn btn-default btn-yellow" href="#" role="button">ADD TO CART</a></div>
                    	</div>
                    </div>
                </div>
            </div>
            <div class="container">
            	<div class="col-sm-12"><hr class="hr-product-footer"></div>
            	<div class="col-sm-6">
				<a class="btn btn-default button-black" href="{{URL::to('product')}}" role="button"><span class="glyphicon glyphicon-th-large"></span> GRID</a>
				&nbsp;<a class="btn btn-default button-black" href="{{URL::to('product_list')}}" role="button"><span class="glyphicon glyphicon-th-list"></span> LIST</a>
  				</div>
                <div class="col-sm-6 text-right">
                                <nav style="display:inline">
  <ul class="pagination pagination-sm">
    <!--<li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>-->
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true"><span class="glyphicon glyphicon-menu-right"></span></span>
      </a>
    </li>
  </ul>
</nav>
                </div>
            </div>
        </div>
    @endsection