@extends('layout-frontend')
    @section('content')
        <div id="wrapper">
            <div class="container about-banner">
				<div class="col-xs-12">Home<span class="txt-black"> > About Us</span><hr class="hr-footer"></div>
            </div>
            <div class="container about-banner2">
                <div class="col-xs-12"><img src="{{ URL::asset('images/about-banner.jpg')}}" alt="" class="img-responsive"/></div>         
            </div>
            <div class="container">
                <div class="col-sm-4"><img src="{{ URL::asset('images/about-banner-01.jpg')}}" alt="" class="img-responsive" vspace="20"/></div>
                <div class="col-sm-4"><img src="{{ URL::asset('images/about-banner-02.jpg')}}" alt="" class="img-responsive" vspace="20"/></div> 
                <div class="col-sm-4"><img src="{{ URL::asset('images/about-banner-03.jpg')}}" alt="" class="img-responsive" vspace="20"/></div>         
            </div>
            <div class="container about-content">
            	<div class="col-md-6 about-content-inner"><span class="about-content-head">About Us</span><br>ร้านลอฟท์ได้ถือกำเนิดที่ประเทศญี่ปุ่นในปี 1987 เปิดให้บริการครั้งแรกที่ย่านช๊อปปิ้งชื่อดัง "ชิบุย่า" ในใจกลางมหานครโตเกียว โดยเน้นแนวคิดของร้านสเปเชี่ยลตี้สโตร์ที่เสนอสินค้าที่มีความหลากหลายสำหรับใช้ในชีวิตประจำวัน ในขณะเดียวกัน ร้านลอฟท์ยังสามารถตอบสนองไลฟ์สไตล์ของลูกค้าในหลายกลุ่มได้เป็นอย่างดี สินค้าในร้านลอฟท์มีให้เลือกอย่างมากมาย เปรียบเสมือนได้เดินช๊อปปิ้งอย่างเพลิดเพลินในคลังสินค้าเลยทีเดียว แต่ขณะเดียวกันร้านลอฟท์ยังคงเน้นประสบการณ์การช๊อปปิ้งที่ตื่นเต้น และสนุกกับการเลือกชมสินค้าที่แปลกใหม่ และมีดีไซน์ไม่เหมือนใคร</div>
                <div class="col-md-6 about-content-inner">
                	<div class="col-xs-12 about-content-box">
                	<div class="col-sm-1 hidden-xs">
                    	<div class="row">
                        <img src="{{ URL::asset('images/about-locate-prev.jpg')}}" alt="" class="img-responsive" vspace="110"/>
                        </div>
                    </div>
                    <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
                    	<div class="row">
                        	<center><img src="{{ URL::asset('images/about-locate-prev.jpg')}}" alt="" class="img-responsive" vspace="10"/></center>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <span class="about-content-head2">Zpell @ Future Park</span><br><img src="{{ URL::asset('images/about-locate-01.jpg')}}" alt="" class="img-responsive"/><br>1St Floor<br>989, Phaholyothin Road,<br>Thanyaburi, Pathumthani 12130<br><span class="about-content-head3">Opening hour:</span><br> Mon-Thu 10:30-21:30<br>Fri: 10:30-22:00<br>Sat-Sun / Public Holiday<br>10:00-22:00<br>Tel. 02-150-9028
                    </div>
                    <div class="col-xs-12 col-sm-5 hidden-xs">
                        <span class="about-content-head2">Siam Center</span><br><img src="{{ URL::asset('images/about-locate-02.jpg')}}" alt="" class="img-responsive"/><br>4th Floor<br>989, Rama 1 Road,<br>Pathumwan, Bangkok 10330<br><span class="about-content-head3">Opening hour:</span><br>Everyday 10:00-22:00<br>Tel. 02-658-0328-30
                    </div>
                    <div class="col-sm-1 hidden-xs">
                    	<div class="row">
                        <img src="{{ URL::asset('images/about-locate-next.jpg')}}" alt="" class="img-responsive" vspace="110"/>
                        </div>
                    </div>
                    <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
                    	<div class="row">
                        	<center><img src="{{ URL::asset('images/about-locate-next.jpg')}}" alt="" class="img-responsive" vspace="10"/></center>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    @endsection