<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Loft</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('css/signin.css') }}" />
</head>

<body>
<div id="signin">
    <div id="logo">
        <img src="{{ URL::asset('images/logo.gif') }}" />
    </div>

    <div class="headline">
        Sign In
    </div>

    {{ Form::open(array('url' => 'signin', 'method' => 'post', 'class' => 'form')) }}
    <div class="row">
        <label for="email">E-mail: </label>
        <div class="value">
            <input type="email" id="email" name="email" placeholder="E-mail" class="form-control" />
        </div>
    </div>

    <div class="row">
        <label for="password">Password: </label>
        <div class="value">
            <input type="password" id="password" name="password" placeholder="Password" class="form-control"  />
        </div>
    </div>

    @if (!empty($err_msg))
    <div class="row err_msg">
        {{ $err_msg }}
    </div>
    @endif

    <div class="row text-center">
        <button type="submit" class="btn submit-btn">Sign In</button>
    </div>
    {{ Form::close() }}
</div>
</body>
</html>