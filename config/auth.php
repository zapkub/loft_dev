<?php

return [
    'defaults' => [
        'guard' => 'user',
        'passwords' => 'user',
    ],
    'guards' => [
        'user' => [
            'driver' => 'session',
            'provider' => 'user',
        ],
        'admin' => [
            'driver' => 'session',
            'provider' => 'admin',
        ],
    ],
    'providers' => [
        'user' => [
            'driver' => 'eloquent',
            'model' => 'App\Model\User',
        ],
        'admin' => [
            'driver' => 'eloquent',
            'model' => 'App\Model\Admin',
        ],
    ],
    'passwords' => [
        'user' => [
            'provider' => 'user',
            'email' => 'auth.emails.password',
            'table' => 'password_resets',
            'expire' => 60,
        ],
        'admin' => [
            'provider' => 'admin',
            'email' => 'auth.emails.password',
            'table' => 'password_resets',
            'expire' => 60,
        ]
    ]
];
