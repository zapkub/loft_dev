<?php
use App\Http\Middleware\Cors;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::get('/',function(){
//    return view('frontend.index');
//});

Route::get('/', function(){
    return File::get(public_path() . '/dist/index.html');
});
Route::get('about',function(){
    return view('frontend.about');
});
Route::get('product',function(){
    return view('frontend.product');
});
Route::get('product_list',function(){
    return view('frontend.product-list');
});
Route::get('product_detail',function(){
    return view('frontend.product-detail');
});
Route::get('promotion',function(){
    return view('frontend.promotion');
});
Route::get('news',function(){
    return view('frontend.news');
});
Route::get('member',function(){
    return view('frontend.member');
});

Route::get('signin', function(){
    return view('signin');
});
Route::post('signin', 'Frontend\SignInController@postSignIn');


Route::group(array('middleware' => ['cors','web'], 'before' => '', 'prefix' => 'api'), function(){

    Route::get('/','Frontend\HomeController@getIndex');
    Route::resource('category', 'Frontend\CategoryController');
    Route::resource('product', 'Frontend\ProductController');

    Route::get('token', 'Frontend\HomeController@getToken');
    Route::post('signin', 'Frontend\SignInController@postSignIn');
    Route::post('signout', 'Frontend\SignInController@postSignOut');
    Route::get('session', 'Frontend\SignInController@viewSession');

    Route::get('cart', 'Frontend\ShopCartController@getCart');
    Route::post('cart/add', 'Frontend\ShopCartController@addToCart');

    Route::post('apply_code', 'Frontend\ShopCartController@applyCode');

    Route::get('category/product/{cat_url}/{page}/{limit}','Frontend\ProductController@getProductByCategory');
    Route::get('category/product/{cat_url}/{subcat_url}/{page}/{limit}','Frontend\ProductController@getProductBySubCategory');

    Route::controller('location','Frontend\LocationController');
    Route::get('staticpage/{id}','Frontend\StaticPageController@getIndex');

    Route::get('contactfrom','Frontend\ContactController@getIndex');
    Route::post('contactfrom','Frontend\ContactController@postContactFrom');    // should be Contact Form

    Route::controller('news','Frontend\NewsController');

    Route::controller('subscribe','Frontend\SubscribeController');

    Route::controller('paysbuy','Frontend\PaysbuyController');

    Route::group(['prefix'=>'shop'],function() {
        Route::resource('product', 'Frontend\ShopProductController');
        Route::controller('cart', 'Frontend\ShopCartController');
    });
});


/*Route::get('/','Frontend\HomeController@getIndex' );*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});


Route::group(['prefix'=>'_admin'],function() {

    Route::get('/',['middleware'=>'admin',function(){
        return view('backend.index');
    }]);
    Route::get('login','Backend\AuthController@getLogin');
    Route::post('login','Backend\AuthController@postLogin');
    Route::get('logout','Backend\AuthController@getLogout');

    Route::resource('new_page', 'Backend\NewPageController');
    Route::resource('user', 'Backend\UserController');
    Route::resource('address_book', 'Backend\AddressBookController');
    Route::resource('orders', 'Backend\OrdersController');
    Route::resource('orders_detail', 'Backend\OrdersDetailController');
    Route::resource('category', 'Backend\CategoryController');
    Route::resource('product_category', 'Backend\ProductCategoryController');
    Route::resource('product_attribute', 'Backend\ProductAttributeController');
    Route::resource('manufacturer', 'Backend\ManufacturerController');
    Route::resource('product_option_group', 'Backend\ProductOptionGroupController');
    Route::resource('attribute', 'Backend\AttributeController');
    Route::resource('special', 'Backend\SpecialController');
    Route::resource('product_option', 'Backend\ProductOptionController');
    Route::resource('discount', 'Backend\DiscountController');
    Route::resource('discount_code', 'Backend\DiscountCodeController');
    Route::resource('review', 'Backend\ReviewController');
    Route::resource('banner_promotion', 'Backend\BannerPromotionController');
    Route::resource('about_us', 'Backend\AboutUsController');
    Route::resource('contact_us', 'Backend\ContactUsController');
    Route::resource('blog', 'Backend\BlogController');
    Route::resource('blog_product', 'Backend\BlogProductController');
    Route::resource('blog_tag', 'Backend\BlogTagController');
    Route::resource('contact_msg', 'Backend\ContactMsgController');

    Route::resource('product', 'Backend\ProductController');
    Route::resource('option_group', 'Backend\OptionGroupController');
    Route::resource('option', 'Backend\OptionController');
    Route::resource('tag', 'Backend\TagController');
    Route::resource('product_tag', 'Backend\ProductTagController');
    Route::controller('/change','Backend\ChangeStatusController');
    Route::controller('/change','Backend\ChangeStatusController');

    Route::post('status-change','Backend\OrdersController@chk_status');

    Route::post('api-active','Backend\ApiController@changeActive');
    Route::controller('/sequence','Backend\SequenceController');





});