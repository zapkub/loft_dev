<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Library\MainFunction;
use Carbon\Carbon;
use App\Model\Subscribe;
use DB;
use Illuminate\Support\Facades\Response;
use Session;
use Input;
use Cookie;
use Illuminate\Cookie\CookieJar;

class SignInController extends Controller
{

    public function __construct()
    {

    }
    public function postSignIn(Request $request){
        $email = $request->email;
        $password = $request->password;
        $data = User::where('email',$email)->where('password', $password)->first();

        if(empty($data)){
            $err_msg = 'The email or password is incorrect.';

            return response()->json(array('msg' => $err_msg, 'status' => 0));

//            return view('signin', compact('err_msg'));
        }else{
//           $token = csrf_token();
//           session()->put(['my_token' => $token]);
           session()->put('s_user_id', $data->user_id);
//
//            return session()->all();

//            $minutes = 36000;
//            Cookie::queue('laravel_session', 'develop');
//            Cookie::queue('s_user_id', $data->user_id, $minutes);
//            Cookie::queue('s_user_firstname', $data->firstname, $minutes);
//            Cookie::queue('s_user_lastname', $data->lastname, $minutes);
//            Cookie::queue('s_user_email', $data->email, $minutes);

            return response()->json(array('status' => 1,'user_id'=>$data->user_id));

//            return Redirect::to('/');
        }
    }

    public function postSignOut(){
        session()->flush();

        return response()->json(array('status' => 1));
    }

    public function viewSession(){
        return response()->json(array('session' => session()->all()));
    }

    public function postStupidCart(Request $request){

        $user_id = $request->user_id;
        $product_id = $request->product_id;
        $amount = $request->amount;

        return response()->json(array('user_id' => $user_id, 'product_id' => $product_id, 'amount' => $amount));

       // return session()->all();

//        return $token . " / " . Session::get('my_token') . ' / ' . Session::get('user_id');
    }



}