<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use Carbon\Carbon;
use App\Model\News;

use DB;
use Input;

class NewsController extends Controller
{
    public function __construct()
    {

    }
    public function getIndex(){
        $data = News::where('status','1')->orderBy('created_at','desc')->get();

        return response()->json($data);
    }

    public function getDetail($id){
        $data = News::where('status','1')->where('news_id',$id)->get();

        return response()->json($data);
    }
}