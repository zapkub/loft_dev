<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use Carbon\Carbon;
use App\Model\BannerTop;
use App\Model\BannerPromotion;
use App\Model\Product;
use DB;
use Input;
class PaysbuyController extends Controller
{

    public function __construct()
    {

    }
    public function getIndex(){
        $account = "FNPT@siamism.com";
        $invoice = "INV-2016031100001";
        $description = "ชำระค่าสินค้า";
        $price ="1";
        $postURL ="http://localhost/loft.bkksol.com/public/paysbuy/result.php";

        return view('frontend.paysbuy',compact('account','invoice','description','price','postURL'));
    }

}