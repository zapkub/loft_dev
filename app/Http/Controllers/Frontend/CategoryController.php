<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Model\Category;
use DB;
use Input;

class CategoryController extends Controller
{
    public function __construct()
    {

    }
    public function index(){
        $categories = Category::where('parent_category_id', '0')->get();

        foreach ($categories as $cat){
            $cat->url = str_slug(strtolower($cat->category_name), '-');
            $cat->sub_categories = Category::where('parent_category_id', $cat->category_id)->get();

            foreach ($cat->sub_categories as $sub_cat) {
                $sub_cat->url = str_slug(strtolower($sub_cat->category_name), '-');
            }

        }

        return response()->json($categories);
    }

    public function show($id){
        $category = Category::find($id);
        $category->url = str_slug(strtolower($category->category_name), '-');
        $category->sub_categories = Category::where('parent_category_id', $category->category_id)->get();
        foreach ($category->sub_categories as $sub_cat) {
            $sub_cat->url = str_slug(strtolower($sub_cat->category_name), '-');
        }

        return response()->json($category);
    }
}