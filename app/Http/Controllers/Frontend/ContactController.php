<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Model\ContactFrom;
use DB;
use Input;

class ContactController extends Controller
{
    public function __construct()
    {

    }
    public function getIndex(){
        $contact_from = ContactFrom::get();
        return response()->json($contact_from);
    }
    public function postContactFrom(Request $request){
        $data = new ContactFrom();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->message = e($request->message);
        $data->save();
        return 'done';
    }
}