<?php
namespace App\Http\Controllers\Frontend;

use App\Model\Contact;
use Illuminate\Http\Request;
use App\Library\MainFunction;
use App\Model\Location;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class HomeController extends Controller
{

    public function __construct()
    {

    }
    public function getIndex(){
        return view('index');
    }

    public function getToken(){
        return csrf_token();
    }

}