<?php
namespace App\Http\Controllers\Frontend;
use App\Model\OptionGroup;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use Carbon\Carbon;

use App\Model\Category;
use App\Model\Product;
use App\Model\ProductOption;
use App\Model\ProductOptionGroup;
use App\Model\ProductCategory;
use App\Model\Gallery;
use App\Model\RelatedProduct;
use App\Model\Review;
use App\Model\Tag;

use DB;
use Input;

class ProductController extends Controller
{
    public function __construct()
    {

    }
    public function index(){
        $objProduct = new Product();
        $joinSpecial = $objProduct->JoinSpecial();

        $products = Product::select('product.*','special.special_id','special.price')
            ->leftJoin(DB::raw($joinSpecial),'special.product_id','=','product.product_id')
            ->where('product.status','1')
            ->get();

        foreach ($products as $product){
            $product->url = str_slug($product->product_name, '-');
        }

        return response()->json($products);
    }

    public function getProductByCategory($url, $page, $limit){
        $categories = Category::where('parent_category_id', '0')->get();

        foreach ($categories as $cat){
            $cat_url = str_slug(strtolower($cat->category_name), '-');
            if ($cat_url == $url){
                $cat_id = $cat->category_id;
                break;
            }
        }
        if (!isset($cat_id))
            return response()->json(array('msg'=>'category not found'));

        $count_products = ProductCategory::join('product','product.product_id','=','product_category.product_id')
            ->where("category_id", $cat_id)->count();
        $products = ProductCategory::join('product','product.product_id','=','product_category.product_id')
            ->where("category_id", $cat_id)
            ->skip(($page-1)*$limit)->take($limit)
            ->get();

        foreach ($products as $product){
            $product->url = str_slug($product->product_name, '-');
        }

        if (count($products) == 0)
            return response()->json(array('msg'=>'no data'));
        else
            return response()->json(array('count_pages' => (intval(abs($count_products-1)/$limit))+1,'products' => $products));
    }

    public function getProductBySubCategory($cat_url, $subcat_url, $page, $limit){
        $categories = Category::where('parent_category_id', '0')->get();

        foreach ($categories as $cat){
            $x_cat_url = str_slug(strtolower($cat->category_name), '-');
            if ($x_cat_url == $cat_url){
                $cat_id = $cat->category_id;
                break;
            }
        }

        if (!isset($cat_id))
            return response()->json(array('msg'=>'category not found'));

        $subcategories = Category::where('parent_category_id', $cat_id)->get();

        foreach ($subcategories as $sub_cat){
            $x_subcat_url = str_slug(strtolower($sub_cat->category_name), '-');
            if ($x_subcat_url == $subcat_url){
                $subcat_id = $sub_cat->category_id;
                break;
            }
        }

        if (!isset($subcat_id))
            return response()->json(array('msg'=>'sub-category not found'));

        $count_products = ProductCategory::join('product','product.product_id','=','product_category.product_id')
            ->where("category_id",$subcat_id)->count();
        $products = ProductCategory::join('product','product.product_id','=','product_category.product_id')
            ->where("category_id",$subcat_id)
            ->skip(($page-1)*$limit)->take($limit)
            ->get();

        foreach ($products as $product){
            $product->url = str_slug($product->product_name, '-');
        }

        if (count($products) == 0)
            return response()->json(array('msg'=>'no data'));
        else
            return response()->json(array('count_pages' => (intval(abs($count_products-1)/$limit))+1, 'products' => $products));
    }

    public function show($id){
        $objProduct = new Product();
        $joinSpecial = $objProduct->JoinSpecial($id);

        // Get Product Detail
        $product = Product::select('product.*','manufacturer.name as manufacturer_name','special.special_id','special.price')
            ->leftJoin(DB::raw($joinSpecial),'special.product_id','=','product.product_id')
            ->leftJoin('manufacturer','manufacturer.manufacturer_id','=','product.manufacturer_id')
            ->where('product.product_id',$id)
            ->where('product.status','1')
            ->first();

        $productId = $product->product_id;

        // Get Galleries
        $galleries = Gallery::where('product_id', $productId)->get();
        $product->gallery = $galleries;

        // Get product option group id
        $productOptionGroup = ProductOptionGroup::where('product_id',$id)->where('require','1')->orderBy('product_option_group_id','desc')->take(1)->first();
        $product->product_option_group = array();

        if(count($productOptionGroup)){
            $option_group_id = $productOptionGroup->option_group_id;
            $product_option_group = $productOptionGroup;
            // Get option group name
            $groupName = OptionGroup::select('name as group_name')->where('option_group_id',$option_group_id)->first();
            // Get product option
            $productOption = ProductOption::select('product_option.product_option_id','product_option.option_id','option.name as option_name','product_option.price','product_option.qty')
                ->join('option','option.option_id','=','product_option.option_id')
                ->where('product_option.product_id',$id)
                ->where('option.option_group_id',$option_group_id)
                ->get();

            if(count($productOption) > 0){
                $product->product_option_group = $groupName;
                $product->product_option_group->option = $productOption;
            }
        }

        // Get Related Product
        $related = RelatedProduct::select('product.product_id', 'product.product_name', 'product.retail_price', 'special.price')
            ->join('product', 'product.product_id', '=', 'related_product.related_id')
            ->leftJoin(DB::raw($joinSpecial), 'special.product_id', '=', 'product.product_id')
            ->where('related_product.product_id', $productId)
            ->orderByRaw('RAND()')
            ->take(3)
            ->get();
        $product->related_product = $related;

        // Get Review
        $reviews = Review::where('product_id',$productId)->where('status','1')->get();
        $product->review = $reviews;

        // Get Tags
//        $tags = Tag::where('table_name', 'product')->where('pk_id', $productId)->get();
//        $product->tags = $tags;

        // Get Product URL
        $product->url = str_slug(strtolower($product->product_name), '-');

        return response()->json($product);
    }

}