<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Blog;
use App\Model\BlogTag;
use App\Model\Tag;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class BlogController extends Controller
{
    public $model = 'App\Model\Blog';
    public $titlePage = 'Blog';
    public $tbName = 'blog';
    public $pkField = 'blog_id';
    public $fieldList = array('title_th','title_en','author_id', 'post_datetime', 'img_name', 'thumb_img_name', 'msg_th', 'msg_en');
    public $a_search = array('title_th', 'title_en');
    public $path = '_admin/blog';
    public $page = 'blog';
    public $viewPath = 'backend/blog';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if (empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if (empty($sortBy)) $sortBy = 'desc';
        $search = Input::get('search');


        $blog_chk = $request->input('blog_chk');

        $tag_name = Input::get('tag_name');

        if (!empty($tag_name)) {

            $tag = Tag::where('tag_name', $tag_name)->first();
            if (count($tag) == 0) {

                $set_tag = new Tag();
                $set_tag->tag_name = $tag_name;
                $set_tag->save();


            }

        }

        if (!empty($blog_chk)) {
            foreach ($blog_chk as $b_tag) {

                if (empty($tag_name)) {

                    return redirect()->back();
                }

                $tag_find = Tag::where('tag_name', $tag_name)->first();
                $set_data = Tag::find($tag_find->tag_id);
                $blog = BlogTag::where('blog_id', $b_tag)->where('tag_id', $set_data->tag_id)->first();

                if (count($blog) == 0) {
                    $product_tag = new BlogTag();
                    $product_tag->blog_id = $b_tag;
                    $product_tag->tag_id = $set_data->tag_id;
                    $product_tag->save();
                }
            }
            return redirect()->back();
        }



        $model = $this->model;


        $data = new $model;


        if (!empty($search)) {
            $data = $data->where(function ($query) use ($search) {
                foreach ($this->a_search as $field) {
                    $query = $query->orWhere($field, 'like', '%' . $search . '%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy, $sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath . '/index', compact('data', 'countData'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath . '/update', compact('url_to', 'method', 'txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();

        $strParam = $request->strParam;
        $model = $this->model;
        $data = new $model;
        $id = $objFn->db_add($data, $this->pkField, $request, $this->fieldList);

        if (Input::hasFile('img_path')) { // เพิ่มตรงนี้
            $photo = $request->file('img_path');                    // get image from form
            $new_name = date('YmdHis') . '-' . rand(10000, 99999);                             // set new name
            $path = public_path('uploads/blog_img_name');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_name = $filename;
            $data->save();
        }

        if (Input::hasFile('img_path1')) { // เพิ่มตรงนี้
            $photo = $request->file('img_path1');                    // get image from form
            $new_name = date('YmdHis') . '-' . rand(10000, 99999);                             // set new name
            $path = public_path('uploads/blog_thumb');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $model::find($id);
            $data->thumb_img_name = $filename;
            $data->save();
        }


        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path . '/' . $id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl', URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);

        $id = $objFn->db_update($data, $this->pkField, $request, $this->fieldList); // add $id

        if (Input::hasFile('img_path')) { // test P.
            $photo = $request->file('img_path');                    // get image from form
            $new_name = date('YmdHis') . '-' . rand(10000, 99999);                             // set new name
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/blog_img_name');           // set path
            $objFn->del_storage($path, $old_name);                   // delete old picture in storage
            $objFn->del_storage($path . '/100/', $old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_name = $filename;
            $data->save();
        }

        if (Input::hasFile('img_path1')) { // test P.
            $photo = $request->file('img_path1');                    // get image from form
            $new_name = date('YmdHis') . '-' . rand(10000, 99999);                             // set new name
            $old_name = $data->thumb_img_name;                            // get old name
            $path = public_path('uploads/blog_thumb');           // set path
            $objFn->del_storage($path, $old_name);                   // delete old picture in storage
            $objFn->del_storage($path . '/100/', $old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $model::find($id);
            $data->thumb_img_name = $filename;
            $data->save();
        }




        return Redirect::to($this->path . '?' . $strParam);



    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl', URL::previous());
        $model = $this->model;

        $data = $model::select("img_name", "thumb_img_name")
            ->where("blog_id", $id)
            ->first();

        $path = ('upload/blog_img_name/');           //// set path
        $path2 = ('upload/blog_img_name/100/');           //// set path
        $path3 = ('upload/blog_thumb/');           //// set path
        $path4 = ('upload/blog_thumb/100/');           //// set path

        $file = "$data->img_name";
        $file2 = "$data->thumb_img_name";


        if ($file != '') {
            if (unlink($path.$file)) {                    // delete image in public path
            }
            if (unlink($path2.$file)) {                   // delete resize image
            }
        }
        if ($file2 != '') {
            if (unlink($path3.$file2)) {                    // delete image in public path
            }
            if (unlink($path4.$file2)) {                   // delete resize image


            }
        }
        else {

        }

            $model::find($id)->delete();

            return Redirect::to(Session::get('referUrl'));

        }

}

