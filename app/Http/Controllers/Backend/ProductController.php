<?php
namespace App\Http\Controllers\Backend;

use App\Model\ProductOptionGroup;
use App\Model\ProductTag;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\StockStatus;
use App\Model\WeightClass;
use App\Model\Tag;
use App\Model\DimensionClass;
use App\Model\Manufacturer;
use App\Model\OptionGroup;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class ProductController extends Controller
{
    public $model = 'App\Model\Product';
    public $model2 = 'App\Model\StockStatus';
    public $model3 = 'App\Model\WeightClass';
    public $model4 = 'App\Model\DimensionClass';
    public $model5 = 'App\Model\Manufacturer';
    public $titlePage = 'Product';
    public $tbName = 'product';
    public $pkField = 'product_id';
    public $fieldList = array('product_name','product_name_th','detail','photo','retail_price','minimum_qty','qty','status','meta_title','meta_keyword','meta_description','model','sku','ean','available_date','dimension_l','dimension_w','dimension_h','dimension_class_id','weight','weight_class_id','manufacturer_id','stock_status_id');
    public $a_search = array('product_name');
    public $path = '_admin/product';
    public $page = 'product';
    public $viewPath = 'backend/product';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';
        $stock_status_id = Input::get('stock_status_id');
        $option_group_id = Input::get('option_group_id');
        $search = Input::get('search');

        $product_chk = $request->input('product_chk');

        $tag_name = Input::get('tag_name');

        if (!empty($tag_name)) {

            $tag = Tag::where('tag_name', $tag_name)->first();
            if (count($tag) == 0) {

                $set_tag = new Tag();
                $set_tag->tag_name = $tag_name;
                $set_tag->save();


            }

        }

            if (!empty($product_chk)) {
                foreach ($product_chk as $p_tag) {

                    if (empty($tag_name)) {
                        
                        return redirect()->back();
                    }

                    $tag_find = Tag::where('tag_name', $tag_name)->first();
                    $set_data = Tag::find($tag_find->tag_id);
                    $product = ProductTag::where('product_id', $p_tag)->where('tag_id', $set_data->tag_id)->first();

                    if (count($product) == 0) {
                        $product_tag = new ProductTag();
                        $product_tag->product_id = $p_tag;
                        $product_tag->tag_id = $set_data->tag_id;
                        $product_tag->save();
                    }
                }
                    return redirect()->back();
            }




        $model = $this->model;

        $data = new $model;

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();



        return view($this->viewPath.'/index',compact('data','countData','stock_status_id','option_group_id'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $model2 = $this->model2;
        $data2 = $model2::all();

//        $model3 = $this->model3;
        $weight1 = WeightClass::all();

        $model4 = $this->model4;
        $dimension = $model4::all();

        $model5 = $this->model5;
        $manufacturer = $model5::all();




        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','data2','weight1','dimension','manufacturer'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;

        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('img_path')) { // เพิ่มตรงนี้
            $photo = $request->file('img_path');                    // get image from form
            $path = public_path('uploads/product/');           // set path
            $filename = $photo->getClientOriginalName();               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->photo = $filename;
            $data->save();
        }



        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        $model2 = $this->model2;
        $data2 = $model2::all();

        $model3 = $this->model3;
        $weight1 = $model3::all();

        $model4 = $this->model4;
        $dimension = $model4::all();

        $model5 = $this->model5;
        $manufacturer = $model5::all();


        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','data2','weight1','dimension','manufacturer'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);

        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);


        if (Input::hasFile('img_path')) { // test P.
            $photo = $request->file('img_path');                    // get image from form
            $old_name = $data->photo;                            // get old name
            $path = public_path('uploads/product/');           // set path
            $objFn->del_storage($path,$old_name);                   // delete old picture in storage
            $objFn->del_storage($path.'/100/',$old_name);                   // delete old picture in storage
            $filename = $photo->getClientOriginalName();           // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->photo = $filename;
            $data->save();
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;

        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

