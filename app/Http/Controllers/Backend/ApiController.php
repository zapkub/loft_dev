<?php
namespace App\Http\Controllers\Backend;

use App\Model\ProductCategory;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\User;
use App\Model\Product;
use App\Model\Location;
use App\Model\ContactFrom;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Validator;

class ApiController extends Controller
{

    public function __construct()
    {
//        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page




    public function changeActive()
    {

        $rules = array('pk_field'=>'required','v_pk_field'=>'required','change_field'=>'required','value'=>'required','tb_name'=>'required');
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {


            if(Input::get('value') == '1')
            {
                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 0,
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "0";
            }else{

                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 1,
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "1";
            }
        }
    }
}

