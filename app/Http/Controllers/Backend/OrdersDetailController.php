<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\NewPage;
use App\Model\Orders;
use App\Model\OrdersDetail;
use App\Model\Status;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class OrdersDetailController extends Controller
{
    public $model = 'App\Model\OrdersDetail';
    public $titlePage = 'Orders Detail';
    public $tbName = 'orders_detail';
    public $pkField = 'orders_detail_id';
    public $fieldList = array('orders_id','product_id','product_name','qty','shipped','unit_price');
    public $a_search = array('product_name');
    public $path = '_admin/orders_detail';
    public $page = 'orders_detail';
    public $viewPath = 'backend/orders_detail';

    public $model2 = 'App\Model\Orders';
    public $tbName2 = 'orders';
    public $pkField2 = 'orders_id';
    public $fieldList2 = array('orders_date','parent_orders','user_id', 'billing_address_id', 'billing_address', 'shipping_address_id', 'shipping_address', 'payment_by', 'total', 'discount_code_id','discount', 'status_id');


    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $mainFn = new MainFunction;
        $a_otherParam2 = Input::except([]);
//        print_r($a_otherParam2);
        $strParam2 = $mainFn->parameter($a_otherParam2);

//        $p = Input::except([]);



        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';
        $orders_id = Input::get('orders_id');

        $search = Input::get('search');

        $subtitle = Orders::find($orders_id);

        $model = $this->model;


        $data = new $model;
        $data = $data->where('orders_id',$orders_id);

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData','orders_id','subtitle'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $orders_id = Input::get('orders_id');
        $subtitle = Orders::find($orders_id);

        $model = $this->model;

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','subtitle'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;
        $model = $this->model;
        $model2 = $this->model2;

        $data = new $model;
        $data2 = new $model2;

        $orders = DB::table('orders')->leftJoin('orders_detail','orders.orders_id','=','orders_detail.orders_id')
            ->where('orders.orders_id',$request->orders_id)
            ->select('orders.*','orders_detail.*')
            ->get();

        foreach($orders as $value){
            if($value->qty != $value->shipped){
                $total_shipped = $value->shipped+$request->shipped[$value->orders_detail_id];

                DB::table('orders_detail')
                    ->where('orders_detail_id',$value->orders_detail_id)
                    ->update(['shipped' => $total_shipped]);


                $request->parent_orders = $value->orders_id;
                $request->orders_date = date('Y-m-d');
                $request->user_id = $value->user_id;
                $request->billing_address_id = $value->billing_address_id;
                $request->billing_address = $value->billing_address;
                $request->shipping_address_id = $value->shipping_address_id;
                $request->shipping_address = $value->shipping_address;
                $request->payment_by = $value->payment_by;
                $request->total = $value->total;
                $request->discount_code_id = $value->discount_code_id;
                $request->discount = $value->discount;
                $request->status_id = $value->status_id;
                $orders_id  = $objFn->db_add($data2,$this->pkField2,$request,$this->fieldList2);

                DB::table('orders_detail')->insert(['orders_id' => $orders_id , 'product_id' => $value->product_id , 'product_name' => $value->product_name , 'qty' => $value->qty , 'shipped' => $request->shipped[$value->orders_detail_id] , 'unit_price' => $value->unit_price ]);
            }else{

                DB::table('orders')
                    ->where('orders_id',$request->orders_id)
                    ->update(['status_id' => '3']);

            }
        }

        return Redirect::to($this->path.'/create?orders_id='.$request->orders_id);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {
        $url_to = $this->path;
        $txt_manage = "Print";
        $model = $this->model2;
        $data = $model::find($id);
        return view($this->viewPath.'/print',compact('data','txt_manage','url_to'));
    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $id = $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

