<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\ProductOptionGroup;
use App\Model\ProductOption;
use App\Model\OptionGroup;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class ProductOptionGroupController extends Controller
{
    public $model = 'App\Model\ProductOptionGroup';
    public $titlePage = 'Product Option Group';
    public $tbName = 'product_option_group';
    public $pkField = 'product_option_group_id';
    public $fieldList = array('product_id','option_group_id','require');
    public $a_search = array('name');
    public $path = '_admin/product_option_group';
    public $page = 'product_option_group';
    public $viewPath = 'backend/product_option_group';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;
$product_id = Input::get('product_id');
$option_group_id = Input::get('option_group_id');

        $data = new $model;
        $data = $model::where('product_id',$product_id);

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData','option_group_id','product_id'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $option_group = OptionGroup::all();

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','option_group'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = new $model;
         $objFn->db_add($data,$this->pkField,$request,$this->fieldList);






        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        $option_group = OptionGroup::all();

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','option_group'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;


        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

