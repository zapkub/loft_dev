<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\BlogProduct;
use App\Model\Blog;
use App\Model\Product;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class BlogProductController extends Controller
{
    public $model = 'App\Model\BlogProduct';
    public $titlePage = 'Blog Product';
    public $tbName = 'blog_product';
    public $pkField = 'blog_product_id';
    public $fieldList = array('blog_id','product_id');
    public $a_search = array('blog_id');
    public $path = '_admin/blog_product';
    public $page = 'blog_product';
    public $viewPath = 'backend/blog_product';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;
$blog_id = Input::get('blog_id');
$product_id = Input::get('product_id');

        $subtitle = Blog::find($blog_id);
        $data = new $model;
        $data = $model::where('blog_id',$blog_id);

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData','blog_id','product_id','subtitle'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $product = Product::all();

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','product'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $strParam = $request->strParam;

        $data = new $model;
         $objFn->db_add($data,$this->pkField,$request,$this->fieldList);






        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $product = Product::all();
        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','product'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

