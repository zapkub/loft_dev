<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\NewPage;
use App\Model\Orders;
use App\Model\Status;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class OrdersController extends Controller
{
    public $model = 'App\Model\Orders';
    public $titlePage = 'Orders';
    public $tbName = 'orders';
    public $pkField = 'orders_id';
    public $fieldList = array('orders_date','parent_orders','user_id', 'billing_address_id', 'billing_address', 'shipping_address_id', 'shipping_address', 'payment_by','payment_date', 'total', 'discount_code_id','discount', 'status_id');
    public $a_search = array('user_id');
    public $path = '_admin/orders';
    public $page = 'orders';
    public $viewPath = 'backend/orders';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if (empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if (empty($sortBy)) $sortBy = 'desc';
$user_id = input::get('user_id');
        $search = Input::get('search');

        $model = $this->model;
        $data = $model::whereNull('deleted_at')->where('parent_orders','0');

        if (!empty($search)) {
            $data = $data->where(function ($query) use ($search) {
                foreach ($this->a_search as $field) {
                    $query = $query->orWhere($field, 'like', '%' . $search . '%');
                }
            });
        }

        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy, $sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

        return view($this->viewPath . '/index', compact('data', 'countData','user_id'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $data2 = Status::all();


        return view($this->viewPath . '/update', compact('url_to', 'method', 'txt_manage','data2'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
        $id = $objFn->db_add($data, $this->pkField, $request, $this->fieldList);


        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path . '/' . $id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl', URL::previous());

        $data2 = Status::all();


        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage','data2'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $id = $objFn->db_update($data, $this->pkField, $request, $this->fieldList);

        return Redirect::to($this->path);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl', URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }

    public function chk_status(Request $request)
    {
        echo $request->check;


//        $model = $this->model;
//
//        $data = $model::find($value);
//        $data->STAUS = "Y";
//        $data->save();


       // return Redirect::to($this->path);

    }

    //-------------------------------------------


//return redirect()->back();


}

