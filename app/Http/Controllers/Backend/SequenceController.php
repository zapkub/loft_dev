<?php
namespace App\Http\Controllers\Backend;

use App\Model\BannerPromotion;


use Illuminate\Http\Request;
use App\Model\NewPage;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class SequenceController extends Controller
{

    public function __construct()
    {
//        $this->middleware('admin');
    }


    public function postBannerPromotion(Request $request)
    {

        $banner_promotion_id = $request->banner_promotion_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = BannerPromotion::where('sorting', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = BannerPromotion::find($banner_promotion_id);
            $change_data2->sorting = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = BannerPromotion::find($data->banner_promotion_id);
        $change_data->sorting = $old_sorting;
        $change_data->save();

        $change_data2 = BannerPromotion::find($banner_promotion_id);
        $change_data2->sorting = $new_sorting;
        $change_data2->save();


        return redirect()->back();


    }
}
    //--------------------------------------------------
