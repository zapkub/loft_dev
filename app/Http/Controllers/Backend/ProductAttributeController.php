<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\ProductAttribute;
use App\Model\Product;
use App\Model\Attribute;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class ProductAttributeController extends Controller
{
    public $model = 'App\Model\ProductAttribute';
    public $titlePage = 'Product Attribute';
    public $tbName = 'product_attribute';
    public $pkField = 'product_attribute_id';
    public $fieldList = array('product_id','attribute_id');
    public $a_search = array('product_id');
    public $path = '_admin/product_attribute';
    public $page = 'product_attribute';
    public $viewPath = 'backend/product_attribute';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $attribute_id = Input::get('attribute_id');
        $product_id = Input::get('product_id');



        $search = Input::get('search');

        $model = $this->model;


        $data = new $model;

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData','attribute_id','product_id'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $attribute1 = Attribute::all();
        $product = Product::all();

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','attribute1','product'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
         $objFn->db_add($data,$this->pkField,$request,$this->fieldList);






        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());


        $attribute1 = Attribute::all();
        $product = Product::all();

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','attribute1','product'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

