<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Option;
use App\Model\OptionGroup;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class OptionController extends Controller
{
    public $model = 'App\Model\Option';
    public $titlePage = 'Option';
    public $tbName = 'option';
    public $pkField = 'option_id';
    public $fieldList = array('option_group_id','name');
    public $a_search = array('name');
    public $path = '_admin/option';
    public $page = 'option';
    public $viewPath = 'backend/option';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;

$option_group_id = Input::get('option_group_id');


        $subtitle = OptionGroup::find($option_group_id);
        $data = new $model;
$data = $model::where('option_group_id',$option_group_id);
        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();



        return view($this->viewPath.'/index',compact('data','countData','option_group_id','subtitle'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $option_group = OptionGroup::all();
        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','option_group'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;

     $objFn->db_add($data,$this->pkField,$request,$this->fieldList);



        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        $option_group = OptionGroup::all();

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','option_group'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);

        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);



        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;

        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

