<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\ProductOption;
use App\Model\Product;
use App\Model\Option;
use App\Model\ProductOptionGroup;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class ProductOptionController extends Controller
{
    public $model = 'App\Model\ProductOption';
    public $titlePage = 'ProductOption';
    public $tbName = 'product_option';
    public $pkField = 'product_option_id';
    public $fieldList = array('product_id','option_id','qty','price','point');
    public $a_search = array('product_id');
    public $path = '_admin/product_option';
    public $page = 'product_option';
    public $viewPath = 'backend/product_option';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

$product_id = Input::get('product_id');
$option_id = Input::get('option_id');
        $model = $this->model;



//        $option_group_name = ProductOptionGroup::find($product_option_group_id);

/*echo $option_group_name;
        return exit();*/

        $data = new $model;
        $data = $model::where('product_id',$product_id);

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

        $option_group_name = ProductOptionGroup::join('option_group','option_group.option_group_id','=','product_option_group.option_group_id')->select('option_group.name')->where('product_id',$product_id)->first();



        return view($this->viewPath.'/index',compact('data','countData','product_id','option_id','option_group_name'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
    $product_id = Input::get('product_id');
        $option = Option::leftjoin('product_option_group','product_option_group.option_group_id','=','option.option_group_id')->select('option.option_id','option.name')->where('product_option_group.product_id',$product_id)->whereNull('product_option_group.deleted_at')->get();


        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','option'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = new $model;
         $objFn->db_add($data,$this->pkField,$request,$this->fieldList);






        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());
        $product_id = Input::get('product_id');
        $option = Option::join('product_option_group','product_option_group.option_group_id','=','option.option_group_id')->select('option.option_id','option.name')->where('product_option_group.product_id',$product_id)->whereNull('product_option_group.deleted_at')->get();
        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','option'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

