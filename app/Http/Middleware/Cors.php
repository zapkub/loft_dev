<?php
/**
 * Created by PhpStorm.
 * User: iMac
 * Date: 3/4/16 AD
 * Time: 8:02 PM
 */

namespace App\Http\Middleware;

use Closure;

class Cors {
    public function handle($request, Closure $next)
    {

//        header("Access-Control-Allow-Origin: http://localhost:3000");
//        header('Access-Control-Allow-Credentials: true');

        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Origin' => 'http://localhost:3000',
            'Access-Control-Allow-Credentials'=>'true',
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, Accept, Authorization, X-Requested-With',
//            'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
//            'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin, X-XSRF-TOKEN'
        ];
        if($request->getMethod() == "OPTIONS") {
            // The client-side application can set only headers allowed in Access-Control-Allow-Headers
            return Response::make('OK', 200, $headers);
        }

        $response = $next($request);
        foreach($headers as $key => $value)
            $response->header($key, $value);

        return $response;
    }
}