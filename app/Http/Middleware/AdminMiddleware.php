<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $c_admin_id = Cookie::get('c_admin_id');

        if(!auth()->guard('admin')->check() && empty($c_admin_id)) {
            return redirect()->to('_admin/login');
        }elseif(auth()->guard('admin')->check()){
            return $next($request);
        }
        if(!empty($c_admin_id)){
            $auth = auth()->guard('admin');
            if ($auth->attempt(['admin_id' => $c_admin_id])) {
                return $next($request);
            }
        }

    }
}
