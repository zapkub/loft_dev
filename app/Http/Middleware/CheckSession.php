<?php
/**
 * Created by PhpStorm.
 * User: iMac
 * Date: 3/4/16 AD
 * Time: 8:02 PM
 */

namespace App\Http\Middleware;

use Closure;

class CheckSession {
    public function handle($request, Closure $next)
    {
        $user_id = $request->user_id;       // dev mode

        if (!$user_id){
            $request->user_id = session()->get('user_id');
        }

        $response = $next($request);

        return $response;
    }
}