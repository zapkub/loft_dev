<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockStatus extends Model
{
    use SoftDeletes;
    protected $table = 'stock_status';
    protected $primaryKey = 'stock_status_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Product(){
        return $this->hasMany('App\Model\Product','stock_status_id');
    }

}

?>