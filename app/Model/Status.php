<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    use SoftDeletes;
    protected $table = 'status';
    protected $primaryKey = 'status_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Status(){
        return $this->hasMany('App\Model\Status' ,'status_id');
    }


}

?>