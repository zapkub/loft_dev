<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactMsg extends Model
{
    use SoftDeletes;
    protected $table = 'contact_msg';
    protected $primaryKey = 'contact_msg_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];



}


?>