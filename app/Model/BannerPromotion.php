<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BannerPromotion extends Model
{
    use SoftDeletes;
    protected $table = 'banner_promotion';
    protected $primaryKey = 'banner_promotion_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>