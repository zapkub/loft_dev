<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'product';
    protected $primaryKey = 'product_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function JoinSpecial($id = null){
        $current_time = Carbon::now();
        $data = "(select `special_id`,`product_id`,`price` from `special` where 1 or ( `start_date` <= '$current_time' and `end_date` >= '$current_time')";
        if($id != null){ $data .= " and `product_id` = '".$id."'"; }
        $data .= " order by `special_id` desc limit 1) as `special` ";
        return $data;
    }
    public function StockStatus(){
        return $this->belongsTo('App\Model\StockStatus' ,'stock_status_id');
    }

    public function WeightClass(){
        return $this->belongsTo('App\Model\WeightClass' ,'weight_class_id');
    }

    public function DimensionClass(){
        return $this->belongsTo('App\Model\DimensionClass' ,'dimension_class_id');
    }

    public function Manufacturer(){
        return $this->belongsTo('App\Model\Manufacturer' ,'manufacturer_id');
    }

    public function ProductTag(){
        return $this->hasMany('App\Model\ProductTag','product_id');
    }

    public function checkQty($amount){
        if ($amount < $this->minimum_qty){
            return array('status' => 0, 'msg' => 'The amount should be greater or equal to ' . $this->minimum_qty);
        }
        else if ($amount > $this->product_qty){
            return array('status' => 0, 'msg' => 'The amount is greater than the number of this item in our stock.');
        }
    }
}

?>