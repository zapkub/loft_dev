<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactFrom extends Model
{
    use SoftDeletes;
    protected $table = 'contact_from';
    protected $primaryKey = 'contact_from_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];





}

?>