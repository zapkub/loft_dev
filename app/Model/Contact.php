<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;
    protected $table = 'contact';
    protected $primaryKey = 'contact_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>