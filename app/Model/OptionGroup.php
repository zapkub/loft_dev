<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OptionGroup extends Model
{
    use SoftDeletes;
    protected $table = 'option_group';
    protected $primaryKey = 'option_group_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Option(){
        return $this->hasMany('App\Model\Option','option_group_id');
    }

    public function Product(){
        return $this->hasMany('App\Model\Product','option_group_id');
    }
}



?>