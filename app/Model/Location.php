<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;
    protected $table = 'location';
    protected $primaryKey = 'location_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];





}

?>