<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OptionType extends Model
{
    use SoftDeletes;
    protected $table = 'option_type';
    protected $primaryKey = 'option_type_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function OptionGroup(){
        return $this->hasMany('App\Model\OptionGroup' ,'option_group_id');
    }
}


?>