<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    use SoftDeletes;
    protected $table = 'orders';
    protected $primaryKey = 'orders_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];







    public function User(){
        return $this->belongsTo('App\Model\User' ,'user_id');
    }
    public function Status(){
        return $this->belongsTo('App\Model\Status' ,'status_id');
    }



}

?>