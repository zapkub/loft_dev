<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductAttribute extends Model
{
    use SoftDeletes;
    protected $table = 'product_attribute';
    protected $primaryKey = 'product_attribute_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Product(){
        return $this->belongsTo('App\Model\Product' ,'product_id');
    }


    public function Attribute(){
        return $this->belongsTo('App\Model\Attribute' ,'attribute_id');
    }


}

?>