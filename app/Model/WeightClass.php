<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WeightClass extends Model
{
    use SoftDeletes;
    protected $table = 'weight_class';
    protected $primaryKey = 'weight_class_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Product(){
        return $this->hasMany('App\Model\Product','weight_class_id');
    }

}

?>