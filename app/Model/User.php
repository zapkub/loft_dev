<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    protected $table = 'user';
    protected $primaryKey = 'user_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];



    public function Orders(){
        return $this->hasMany('App\Model\Orders' ,'user_id');
    }




}

?>