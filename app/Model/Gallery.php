<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    use SoftDeletes;
    protected $table = 'gallery';
    protected $primaryKey = 'gallery_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>