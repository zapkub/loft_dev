<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $table = 'category';
    protected $primaryKey = 'category_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public function CategoryName($id)
    {
$data = $this->select("category_name")->where("category_id",$id)->first();
        $category_name = $data->category_name;
        return $category_name;
    }
}

?>