<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;
    protected $table = 'blog';
    protected $primaryKey = 'blog_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>