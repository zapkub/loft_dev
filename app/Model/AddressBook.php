<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddressBook extends Model
{
    use SoftDeletes;
    protected $table = 'address_book';
    protected $primaryKey = 'address_book_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];



    public function User(){
        return $this->belongsTo('App\Model\User' ,'user_id');
    }


}

?>