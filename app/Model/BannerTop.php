<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BannerTop extends Model
{
    use SoftDeletes;
    protected $table = 'banner_top';
    protected $primaryKey = 'banner_top_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>