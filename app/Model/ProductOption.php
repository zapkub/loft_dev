<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductOption extends Model
{
    use SoftDeletes;
    protected $table = 'product_option';
    protected $primaryKey = 'product_option_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];



    public function ProductOptionGroup(){
        return $this->belongsTo('App\Model\ProductOptionGroup' ,'product_option_group_id');
    }
    public function Product(){
        return $this->belongsTo('App\Model\Product' ,'product_id');
    }
    public function Option(){
        return $this->belongsTo('App\Model\Option' ,'option_id');
    }
    public function checkQty($amount){
        if ($amount > $this->qty) {
            return array('status' => 0, 'msg' => 'The amount is greater than the number of this item in our stock.');
        }
    }
}

?>