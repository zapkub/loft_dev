<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogTag extends Model
{
    use SoftDeletes;
    protected $table = 'blog_tag';
    protected $primaryKey = 'blog_tag_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public function Blog(){
        return $this->belongsTo('App\Model\Blog' ,'blog_id');
    }

    public function Tag(){
        return $this->belongsTo('App\Model\Tag' ,'tag_id');
    }


}


?>