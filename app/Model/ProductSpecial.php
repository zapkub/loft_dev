<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSpecial extends Model
{
    use SoftDeletes;
    protected $table = 'special';
    protected $primaryKey = 'special_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>