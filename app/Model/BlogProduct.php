<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogProduct extends Model
{
    use SoftDeletes;
    protected $table = 'blog_product';
    protected $primaryKey = 'blog_product_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public function Product(){
        return $this->belongsTo('App\Model\Product' ,'product_id');
    }
    public function Blog(){
        return $this->belongsTo('App\Model\Blog' ,'blog_id');
    }
}

?>