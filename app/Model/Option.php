<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Option extends Model
{
    use SoftDeletes;
    protected $table = 'option';
    protected $primaryKey = 'option_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function OptionGroup(){
        return $this->belongsTo('App\Model\OptionGroup' ,'option_group_id');
    }
}


?>