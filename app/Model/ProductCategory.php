<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    use SoftDeletes;
    protected $table = 'product_category';
    protected $primaryKey = 'product_category_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Category(){
        return $this->belongsTo('App\Model\Category' ,'category_id');
    }


    public function Product(){
        return $this->belongsTo('App\Model\Product' ,'product_id');
    }


}

?>