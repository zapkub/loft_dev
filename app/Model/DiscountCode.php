<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountCode extends Model
{
    use SoftDeletes;
    protected $table = 'discount_code';
    protected $primaryKey = 'discount_code_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];



}

?>