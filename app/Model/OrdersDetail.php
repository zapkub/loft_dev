<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersDetail extends Model
{
    use SoftDeletes;
    protected $table = 'orders_detail';
    protected $primaryKey = 'orders_detail_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Orders(){
        return $this->belongsTo('App\Model\Orders' ,'orders_id');
    }

}

?>