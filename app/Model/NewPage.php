<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewPage extends Model
{
    use SoftDeletes;
    protected $table = 'new_page';
    protected $primaryKey = 'new_page_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];








}

?>