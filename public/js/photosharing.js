$(document).ready(function() {
    $('#CATEGORY_ID').on('change',function (){

        var category_id = $('#CATEGORY_ID').val();
        var category_name = $('#CATEGORY_ID option:selected').text();
        var variable = '';
        if(category_id != '') {
            location.href = '/photosharing?CATEGORY_ID='+category_id;
        }else{
            location.href = '/photosharing/';
        }
    });

});

$(document).ready(function() {

    $("#BTN_POST_NEW_PHOTO").on( "click", function() {
        $("#frm_data").valid();
        var input = $('input[type=file]');
        var count_input = input.length;
        if(count_input == 1 ){
            if(input.val() == '' ){
                $('#alert_img').html('* กรุณาเลือกภาพจากอุปกรณ์ของท่าน');
            }else{ $("#frm_data").submit(); }
        }else{ $("#frm_data").submit(); }
    });
});

function removeFunction(count_block){
    if(confirm('คุณต้องการลบรูปภาพใช่หรือไม่')){
        $('#showBlock_'+count_block+',#imgInp_'+count_block).remove();
        var count_input = $('input[type=file]').length;
        if(count_input <= 5){
            $('#upload_file_box').css('display','block');
        }
    }
}
$(document).ready(function() {
    $(this).change(function(){
        $('#alert_img').html('');
        var host_name = 'http://'+window.location.hostname;
        var count_click = parseInt($('#count_click').val())+1;
        var count_input = $('input[type=file]').length;

        // Get File
        var data = $('#imgInp_'+count_click).get(0); // Get element
        var file = data.files[0]; // Get file

        // Set Condition
        var arr_type = ["gif", "png" ,"jpg","jpeg"];
        var type = file.type.substring(6);
        var chk_type = arr_type.indexOf(type);
        var fix_size = 250000;
        var size = file.size;

        // Check Condition
        if (data.files && file) {
            if(chk_type >= 0 && size <= fix_size){
                $('#count_click').val(count_click);
                var next_input = 1+count_click;

                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#show_thum').append('<div id=\"showBlock_'+count_click+'\" class=\"block-image\"><div class=\"ss-thumb\"><img src=\"'+e.target.result+'\" style=\"width:152px; max-width:152px; height:152px; max-height:152px;\" /></div><a href=\"javascript:;\" class=\"removeImg delete-image size13 mid-grey\" onClick=\"removeFunction(\''+count_click+'\');\"><img src=\"'+host_name+'/images/icon-delete.png\" /> ลบภาพ</a></div>');
                }
                reader.readAsDataURL(file);

                if(count_input < 5){
                    $('#inFile').append('<input type=\"file\" id=\"imgInp_'+next_input+'\" name=\"imgInp[]\" value="" style=\"display:none;\" >');
                    $('#upload_file_box').css('display','block');
                }else{
                    $('#inFile').append('<input type=\"file\" id=\"imgInp_'+next_input+'\" name=\"imgInp[]\" value="" style=\"display:none;\">');
                    $('#upload_file_box').css('display','none');
                }

                $('#removeImg').css('display','block');
            }else{
                $('#imgInp_'+count_click).val('');
                if(size > fix_size){ alert ("ขนาดไฟล์ต้องห้ามเกิน 250 kb."); }
                if(chk_type < 0) alert ("นามสกุลของรูปภาพต้อง .gif, .png, .jpg เท่านั้น");
            }
        }
    });

    $("#upload_file ").click(function(){
        var count_click = parseInt($('#count_click').val())+1;
        $("#imgInp_"+count_click).click();
    });
});
