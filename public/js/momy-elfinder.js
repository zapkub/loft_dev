

$("#bt_preview").click(function(){
    window.open('', 'formpopup', 'width=1024,height=700,resizeable,scrollbars');
    $("#frm_data").attr("action", 'page.php');
    $("#frm_data").attr("target", 'formpopup');
    $('#frm_data').submit();
});

    <!-- Chk Upload -->
$(document).ready(function(){
    $("#remove_img").click(function(){
        var r = confirm('Are you sure you want to delete');
        if(r == true){
            $("#IMG_PATH2").removeAttr('src');
            $("#upload").show();
            $("#remove_img").hide();
            $("#IMG_PATH,#IMG_TYPE").val('');
        }else{
            return false;
        }

    });
});

$(function () {
    $("#upload").on("click",function(e){
        var objFile= $("<input>",{
            "class":"file_upload",
            "type":"file",
            "multiple":"true",
            "name":"IMG_PATH",
            "style":"display:none",
            change: function(e){
                var files = this.files
                showThumbnail(files)
                $("#upload").hide();
                $("#remove_img").show();
                $("#IMG_TYPE").val(1);
                $("#IMG_PATH").val('');
            }
        });
        $(this).before(objFile);
        $(".file_upload:last").show().click().hide();
        e.preventDefault();
    });

    function showThumbnail(files){

        //    $("#thumbnail").html("");
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/
            if(!file.type.match(imageType)){
                var i = confirm("สกุลไฟล์ไม่ถูกต้อง");
                if(i==true || i==false){
                    exit();
                }
                continue;
            }


            //var image = document.createElement("img");
            var image = document.getElementById("IMG_PATH2");
            var thumbnail = document.getElementById("thumbnail");
            image.file = file;
            thumbnail.appendChild(image)

            var reader = new FileReader()
            reader.onload = (function(aImg){
                return function(e){
                    aImg.src = e.target.result;
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        } // end for loop

    } // end showThumbnail
});
    <!-- End Upload -->

    <!-- Chk File Manager -->

function chk_file_manager(e){
    if(e==1){
        $("#elfinder_group").hide('slow');
        $("#file_manager").val(0);
    }else{
        $("#elfinder_group").show('slow');
        $("#file_manager").val(1);
    }
}
    <!-- End Chk File Manager -->

    <!-- File Manager -->

    function getUrlParam(paramName) {
        var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
        var match = window.location.search.match(reParam) ;
        return (match && match.length > 1) ? match[1] : '' ;
    }

$().ready(function() {
    var url_txt = 'http://'+window.location.hostname +'/tools/elfinder/php/connector.php' ;
    var file_manager = $('#elfinder').elfinder({
            url : url_txt,  // connector URL (REQUIRED)

        // lang: 'ru',             // language (OPTIONAL)
        getFileCallback: function(url) { // editor callback
        //$('#IMG_PATH').val('<?=$_SERVER["SERVER_NAME"]?>'+ url.substr(9));
        $('#IMG_PATH').val(url)
        $('#IMG_TYPE').val('2');
        $('#remove_img,#upload').show();
        $('#IMG_PATH2').attr('src', url);
        get_images_file();
        //FileBrowserDialogue.mySubmit(url); // pass selected file path to TinyMCE
    }
}).elfinder('instance');
});

<!-- End File Manager -->
