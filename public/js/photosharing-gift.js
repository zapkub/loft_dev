$(document).ready(function($){
    $('.photosharing_like').on('click',function (){
        var photosharing_id = $(this).data('id');
        var like = $(this).data('like');
        var _token = $('#_token').val();
        var str_count = parseInt($(this).data('gift'));


        $.ajax({
            url: '/photosharing/like',
            type : 'post',
            data : {'id': photosharing_id,'_token':_token},
            success : function(data){
                console.log(data);
                if(data=='0')
                {
                    window.location.href = '/signin';
                }else if(data=='create')
                {
                    $('#count-gift-'+photosharing_id).empty().append(str_count+1);
                    $('#photosharing_like-'+photosharing_id).data('gift',str_count+1);
                    $('#photosharing_like-'+photosharing_id).removeClass('text-gray').addClass('text-pink');

                }else if(data == 'delete')
                {
                    $('#count-gift-'+photosharing_id).empty().append(str_count-1);
                    $('#photosharing_like-'+photosharing_id).data('gift',str_count-1);
                    $('#photosharing_like-'+photosharing_id).removeClass('text-pink').addClass('text-gray');
                }

            }
        });
    });
});